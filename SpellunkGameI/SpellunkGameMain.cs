﻿using UIKit;

namespace Spellunk
{
  public class Application
  {
    static void Main(string[] args)
    {
      Inv.iOSShell.AppCenterAppID = "31a86c99-ff74-4f8d-98f3-12db05c0911a";
      Inv.iOSShell.Run(Spellunk.Shell.Install);
    }
  }
}