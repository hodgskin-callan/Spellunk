**Spellunk**

Fun and challenging word puzzle to demonstrate the capabilities of the Invention platform.

This project is completely open source and welcomes community contributions!

* [Spellunk for iOS](mailto:hodgskin.callan@gmail.com) (Apple iTunes via TestFlight, email me to join)
* [Spellunk for Android](https://play.google.com/store/apps/details?id=hodgskin.callan.spellunk) (Google Play Open Beta)
* [Spellunk for Windows](https://www.microsoft.com/en-au/store/p/spellunk/9mvpfh3q9v9z) (Windows Store)
* [Spellunk for Web](https://spellunk.azurewebsites.net/) (Azure App Service)


Callan Hodgskin

mailto:hodgskin.callan@gmail.com