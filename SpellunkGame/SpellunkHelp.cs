﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  internal sealed class HelpScreen
  {
    internal HelpScreen(Application Application)
    {
      this.Screen = Application.NewScreen();
      Screen.Title = "Help";
      Screen.Caption = "CLOSE";

      var Surface = Screen.Surface;

      var LayoutDock = Surface.NewVerticalDock();
      Screen.Content = LayoutDock;
      LayoutDock.Margin.Set(10);

      this.HelpFrame = new HelpFrame(Application, Surface);
      LayoutDock.AddClient(HelpFrame);

      var NavigateDock = Surface.NewHorizontalDock();
      LayoutDock.AddFooter(NavigateDock);
      NavigateDock.Margin.Set(0, 10, 0, 0);

      this.BackButton = Surface.NewFlatButton();
      NavigateDock.AddHeader(BackButton);
      BackButton.Border.Set(1).In(Inv.Colour.DarkGray);
      BackButton.Background.In(Inv.Colour.DimGray);
      BackButton.Padding.Set(5);
      BackButton.SingleTapEvent += () =>
      {
        HelpIndex = BackIndex();

        Load();
      };

      this.BackLabel = Surface.NewLabel();
      BackButton.Content = BackLabel;
      BackLabel.Justify.Left();
      BackLabel.Font.Custom(Theme.LargeSize).SmallCaps().In(Inv.Colour.White);

      this.NextButton = Surface.NewFlatButton();
      NavigateDock.AddFooter(NextButton);
      NextButton.Border.Set(1).In(Inv.Colour.DarkGray);
      NextButton.Background.In(Inv.Colour.DimGray);
      NextButton.Padding.Set(5);
      NextButton.SingleTapEvent += () =>
      {
        HelpIndex = NextIndex();

        Load();
      };

      this.NextLabel = Surface.NewLabel();
      NextButton.Content = NextLabel;
      NextLabel.Justify.Right();
      NextLabel.Font.Custom(Theme.LargeSize).SmallCaps().In(Inv.Colour.White);

      this.HelpIndex = Screen.Application.HelpBook.PageList.IndexOf(Application.HelpBook.HowToPlayPage);

      Screen.Surface.ArrangeEvent += () =>
      {
        var SplitWidth = Math.Min(200, (Surface.Window.Width - LayoutDock.Margin.Left - LayoutDock.Margin.Right) / 2);
        BackButton.Size.SetWidth(SplitWidth);
        NextButton.Size.SetWidth(SplitWidth);
      };
    }

    public void Execute(HelpPage HelpPage)
    {
      HelpIndex = Screen.Application.HelpBook.PageList.IndexOf(HelpPage);

      Execute();
    }
    public void Execute()
    {
      Load();

      Screen.Open();
    }

    private void Load()
    {
      if (HelpIndex >= 0 && HelpIndex < Screen.Application.HelpBook.PageList.Count)
      {
        HelpFrame.Load(Screen.Application.HelpBook.PageList[HelpIndex]);

        BackLabel.Text = "< " + Screen.Application.HelpBook.PageList[BackIndex()].Title;
        BackButton.Visibility.Set(HelpIndex > 0);

        NextLabel.Text = Screen.Application.HelpBook.PageList[NextIndex()].Title + " >";
        NextButton.Visibility.Set(HelpIndex < Screen.Application.HelpBook.PageList.Count - 1);
      }
    }
    private int BackIndex()
    {
      var Result = HelpIndex - 1;

      if (Result < 0)
        Result = Screen.Application.HelpBook.PageList.Count - 1;

      return Result;
    }
    private int NextIndex()
    {
      var Result = HelpIndex + 1;

      if (Result >= Screen.Application.HelpBook.PageList.Count)
        Result = 0;

      return Result;
    }

    private readonly Screen Screen;
    private readonly HelpFrame HelpFrame;
    private readonly Inv.Button BackButton;
    private readonly Inv.Label BackLabel;
    private readonly Inv.Button NextButton;
    private readonly Inv.Label NextLabel;
    private int HelpIndex;
  }

  internal sealed class HelpFrame : Inv.Panel<Inv.Frame>
  {
    internal HelpFrame(Application Application, Inv.Surface Surface)
    {
      this.Application = Application;
      this.Surface = Surface;
      this.Base = Surface.NewFrame();

      var LayoutDock = Surface.NewVerticalDock();
      Base.Content = LayoutDock;

      this.TitleLabel = Surface.NewLabel();
      LayoutDock.AddHeader(TitleLabel);
      TitleLabel.Margin.Set(0, 10, 0, 0);
      TitleLabel.Font.Custom(35).SmallCaps();

      this.TileStack = Surface.NewHorizontalStack();
      LayoutDock.AddHeader(TileStack);
      TileStack.Alignment.StretchCenter();
      TileStack.Margin.Set(10);

      this.Memo = Surface.NewMemo();
      LayoutDock.AddClient(Memo);
      Memo.Alignment.TopStretch();
      Memo.IsReadOnly = true;
      Memo.Background.In(Inv.Colour.Transparent);
      Memo.Font.Custom(15);
    }

    public void Load(HelpPage HelpPage)
    {
      TitleLabel.Text = HelpPage.Title;
      Memo.Text = HelpPage.Text;
      
      TileStack.RemovePanels();

      foreach (var Tile in HelpPage.TileList)
      {
        var TileFrame = new TileControl(Application, Surface, Tile);
        TileStack.AddPanel(TileFrame);
      }
    }

    private readonly Application Application;
    private readonly Inv.Surface Surface;
    private readonly Inv.Memo Memo;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Stack TileStack;
  }

  internal sealed class HelpBook
  {
    internal HelpBook(Application Application)
    {
      this.StylePageDictionary = new Dictionary<Style, HelpPage>();
      this.PageList = new Inv.DistinctList<HelpPage>();

      this.HowToPlayPage = AddPage();
      HowToPlayPage.Title = "How to play";
      HowToPlayPage.Text =
        "Words can be formed left-to-right and top-to-bottom but not backwards or diagonally." +
        "Every word you form scores points, and the more points you score, the more moves you have to keep forming words. " +
        "When a word is formed, new tiles are dropped down. " +
        "You must form a specific number of words to complete each level. " +
        "The game ends when you have no moves of any kind remaining." + Environment.NewLine + Environment.NewLine +
        "Each word must be at least " + Rules.MinimumWordLength + " letters." + Environment.NewLine + Environment.NewLine +
        //"As the game progresses the levels get longer and some challenging tiles will appear. " +
        //"But you will earn special moves and premium tiles to keep you going." + Environment.NewLine + Environment.NewLine +
        "The score for each word is the total value of all the tiles in the word, multiplied by the word length, then multiplied again by the value of all the multiplier tiles in the word. " +
        "Bonus points are given for forming a word in a cascade (+5 points per word) and for forming multiple words at the same time (+15 points).";

      this.SwapMovePage = AddPage();
      SwapMovePage.Title = "Swap Moves";
      SwapMovePage.Text =
        "You begin the game with 20 swap moves. You earn +4 swaps every 250 points." + Environment.NewLine + Environment.NewLine +
        "Swap moves are used to exchange two adjacent tiles, but not diagonally.";

      this.StampMovePage = AddPage();
      StampMovePage.Title = "Stamp Moves";
      StampMovePage.Text =
        "You begin the game with 5 stamp moves. You earn +1 stamps every 250 points." + Environment.NewLine + Environment.NewLine +
        "Stamp moves can only be used on blank wooden tiles. Tap on a blank tile twice to open the A-Z letter selection menu.";

      this.JumpMovePage = AddPage();
      JumpMovePage.Title = "Jump Moves";
      JumpMovePage.Text =
        "You begin the game with 0 jump moves. You earn +1 jumps every 500 points." + Environment.NewLine + Environment.NewLine +
        "Jump moves can be used to exchange two non-adjacent tiles.";

      this.WoodenTilePage = AddTileTypePage(Style.Wooden);
      WoodenTilePage.Text =
        "You can permanently stamp a letter on the wooden tiles. Tap on a wooden tile to assign a letter. Wooden tiles are not worth any points.";
      WoodenTilePage.AddTile(new Tile()
      {
        Style = Style.Wooden,
        Symbol = null
      });

      this.MoltenTilePage = AddTileTypePage(Style.Molten);
      MoltenTilePage.Text =
        "Molten tiles are too hot to touch and cannot be swapped. Molten tiles will cascade down the board when words are formed beneath them. Use a molten tile in a word to clear it. Molten tiles are worth an extra 10 points.";
      MoltenTilePage.AddTile(new Tile()
      {
        Style = Style.Molten,
        Symbol = null
      });

      this.BrokenTilePage = AddTileTypePage(Style.Broken);
      BrokenTilePage.Text =
        "Broken tiles lock into place where they land and cannot be swapped. Use a broken tile in a word to clear it. Broken tiles are worth an extra 20 points.";
      BrokenTilePage.AddTile(new Tile()
      {
        Style = Style.Broken,
        Symbol = null
      });

      this.SilverTilePage = AddTileTypePage(Style.Silver);
      SilverTilePage.Text =
        "Silver tiles are the basic tiles split into four tiers." + Environment.NewLine + Environment.NewLine +
        "Tier 1 tiles are all vowels and are worth 1 point each." + Environment.NewLine + Environment.NewLine +
        "Tier 2 tiles are commonly used consonants and are worth 2 points each." + Environment.NewLine + Environment.NewLine +
        "Tier 3 tiles are slightly more difficult to use consonants and are worth 4 points each." + Environment.NewLine + Environment.NewLine +
        "Tier 4 tiles are rare and challenging letters (J, Q, V, X and Z) and are worth 8 points each.";
      SilverTilePage.AddTile(new Tile()
      {
        Style = Style.Silver,
        Symbol = Application.Bag.SymbolByLetter('A')
      });
      SilverTilePage.AddTile(new Tile()
      {
        Style = Style.Silver,
        Symbol = Application.Bag.SymbolByLetter('L')
      });
      SilverTilePage.AddTile(new Tile()
      {
        Style = Style.Silver,
        Symbol = Application.Bag.SymbolByLetter('M')
      });
      SilverTilePage.AddTile(new Tile()
      {
        Style = Style.Silver,
        Symbol = Application.Bag.SymbolByLetter('Z')
      });

      this.GoldTilePage = AddTileTypePage(Style.Gold);
      GoldTilePage.Text = "Gold tiles are a 2x multiplier on the word score.";
      GoldTilePage.AddTile(new Tile()
      {
        Style = Style.Gold,
        Symbol = null
      });

      this.PlatinumTilePage = AddTileTypePage(Style.Platinum);
      PlatinumTilePage.Text = "Platinum tiles are a 3x multiplier on the word score.";
      PlatinumTilePage.AddTile(new Tile()
      {
        Style = Style.Platinum,
        Symbol = null
      });
    }

    public Inv.DistinctList<HelpPage> PageList { get; private set; }
    public Dictionary<Style, HelpPage> StylePageDictionary { get; private set; }
    public HelpPage HowToPlayPage { get; private set; }
    public HelpPage WoodenTilePage { get; private set; }
    public HelpPage MoltenTilePage { get; private set; }
    public HelpPage BrokenTilePage { get; private set; }
    public HelpPage GoldTilePage { get; private set; }
    public HelpPage PlatinumTilePage { get; private set; }
    public HelpPage SilverTilePage { get; private set; }
    public HelpPage StampMovePage { get; set; }
    public HelpPage SwapMovePage { get; set; }
    public HelpPage JumpMovePage { get; set; }

    private HelpPage AddTileTypePage(Style TileType)
    {
      var Page = AddPage();

      Page.Title = TileType.ToString() + " Tiles";

      StylePageDictionary.Add(TileType, Page);

      return Page;
    }
    private HelpPage AddPage()
    {
      var Page = new HelpPage();

      PageList.Add(Page);

      return Page;
    }
  }

  internal sealed class HelpPage
  {
    internal HelpPage()
    {
      this.TileList = new Inv.DistinctList<Tile>();
    }

    public string Title { get; set; }
    public string Text { get; set; }
    public Inv.DistinctList<Tile> TileList { get; private set; }

    public void AddTile(Tile Tile)
    {
      TileList.Add(Tile);
    }
  }
}
