﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  internal sealed class MenuScreen
  {
    internal MenuScreen(Application Application)
    {
      this.Application = Application;

      this.Screen = Application.NewScreen();
      Screen.Title = "Menu";

      var Surface = Screen.Surface;

      var LayoutDock = Surface.NewVerticalDock();
      Screen.Content = LayoutDock;

      var ButtonScroll = Surface.NewVerticalScroll();
      LayoutDock.AddClient(ButtonScroll);

      this.ButtonStack = Surface.NewVerticalStack();
      ButtonScroll.Content = ButtonStack;
      ButtonStack.Alignment.Center();

      this.FaultButton = AddMenuButton("Please send faults", Inv.Colour.Purple);
      FaultButton.SingleTapEvent += () =>
      {
        Application.SendFaults();

        FaultButton.IsVisible = Application.HasFaults();
      };

      var EndButton = AddMenuButton("End this game", Inv.Colour.DimGray);
      EndButton.SingleTapEvent += () =>
      {
        // TODO: are you sure?

        Screen.Close();
        Application.EndGame();
      };

      var LadderButton = AddMenuButton("High score ladder", Inv.Colour.DarkGray);
      LadderButton.SingleTapEvent += () =>
      {
        // TODO: are you sure?
        Screen.Close();
        Application.ViewLadder(null);
      };

      var ContinueButton = AddMenuButton("Continue playing", Inv.Colour.DarkGreen);
      ContinueButton.SingleTapEvent += () =>
      {
        Screen.Close();
      };

      var MuteButton = Surface.NewFlatButton();
      ButtonStack.AddPanel(MuteButton);
      MuteButton.Alignment.Center();
      MuteButton.Margin.Set(0, 10, 0, 0);
      MuteButton.Size.Set(96);
      MuteButton.Padding.Set(16);
      MuteButton.Corner.Set(32);
      MuteButton.Background.In(Inv.Colour.DarkGray);

      var MuteGraphic = Surface.NewGraphic();
      MuteButton.Content = MuteGraphic;
      RefreshMute();

      void RefreshMute() => MuteGraphic.Image = Application.Settings.Mute ? Resources.Icons.SoundOff : Resources.Icons.SoundOn;

      MuteButton.SingleTapEvent += () =>
      {
        Application.Settings.Mute = !Application.Settings.Mute;
        Application.SaveSettings();

        RefreshMute();
      };

      var AboutButton = Surface.NewFlatButton();
      LayoutDock.AddFooter(AboutButton);
      AboutButton.Padding.Set(10);
      AboutButton.Margin.Set(0, 10, 0, 0);
      AboutButton.Background.In(Inv.Colour.DodgerBlue);
      AboutButton.Elevation.Set(2);
      AboutButton.SingleTapEvent += () => Application.SendFeedback();

      var AboutStack = Surface.NewVerticalStack();
      AboutButton.Content = AboutStack;

      var AuthorLabel = Surface.NewLabel();
      AboutStack.AddPanel(AuthorLabel);
      AuthorLabel.Text = "Callan Hodgskin";
      AuthorLabel.Alignment.Center();
      AuthorLabel.Font.Custom(Theme.LargeSize).In(Inv.Colour.White);

      var EmailLabel = Surface.NewLabel();
      AboutStack.AddPanel(EmailLabel);
      EmailLabel.Justify.Center();
      EmailLabel.Font.Custom(Theme.LargeSize).In(Inv.Colour.WhiteSmoke);
      EmailLabel.Text = "hodgskin.callan@gmail.com";

      var VersionLabel = Surface.NewLabel();
      LayoutDock.AddFooter(VersionLabel);
      VersionLabel.Justify.Center();
      VersionLabel.Background.In(Inv.Colour.Black);
      VersionLabel.Font.Custom(Theme.LargeSize).In(Inv.Colour.DimGray);
      VersionLabel.Text = Application.Version;

#if DEBUG
      var WordsButton = Surface.NewFlatButton();
      LayoutDock.AddHeader(WordsButton);
      WordsButton.Margin.Set(5);
      WordsButton.Size.Set(200, 44);
      WordsButton.Background.In(Theme.BorderColour);

      var WordsLabel = Surface.NewLabel();
      WordsButton.Content = WordsLabel;
      WordsLabel.Justify.Center();
      WordsLabel.Text = "[DEBUG] * Words = 1 *";

      WordsButton.SingleTapEvent += () =>
      {
        Screen.Close();

        Application.Deck.Words = 1;
        Application.RenderGame();
      };

      var BoostButton = Surface.NewFlatButton();
      LayoutDock.AddHeader(BoostButton);
      BoostButton.Margin.Set(5);
      BoostButton.Size.Set(200, 44);
      BoostButton.Background.In(Theme.BorderColour);

      var BoostLabel = Surface.NewLabel();
      BoostButton.Content = BoostLabel;
      BoostLabel.Justify.Center();
      BoostLabel.Text = "[DEBUG] * Boost +500 *";

      BoostButton.SingleTapEvent += () =>
      {
        Application.Story.BoostScore(500);
        Application.RenderGame();
      };
#endif
    }

    public void Execute()
    {
      FaultButton.IsVisible = Application.HasFaults();

      Screen.Open();
    }

    private MenuButton AddMenuButton(string Text, Inv.Colour Colour)
    {
      var Result = new MenuButton(Screen.Surface);

      Result.Text = Text;
      Result.Colour = Colour;

      ButtonStack.AddPanel(Result);

      return Result;
    }

    private readonly Application Application;
    private readonly Screen Screen;
    private readonly Inv.Stack ButtonStack;
    private readonly MenuButton FaultButton;
  }

  internal sealed class MenuButton : Inv.Panel<Inv.Button>
  {
    public MenuButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewFlatButton();
      Base.Background.In(Inv.Colour.DarkRed);
      Base.Margin.Set(0, 20, 0, 0);
      Base.Padding.Set(10);
      Base.Elevation.Set(2);

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.Justify.Center();
      Label.Font.Custom(Theme.MassiveSize).In(Inv.Colour.White);
    }

    public Inv.Colour Colour
    {
      get => Base.Background.Colour;
      set => Base.Background.Colour = value;
    }
    public string Text
    {
      get => Label.Text;
      set => Label.Text = value;
    }
    public bool IsVisible
    {
      get => Base.Visibility.Get();
      set => Base.Visibility.Set(value);
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }
    public event Action ContextTapEvent
    {
      add => Base.ContextTapEvent += value;
      remove => Base.ContextTapEvent -= value;
    }

    private readonly Inv.Label Label;
  }
}
