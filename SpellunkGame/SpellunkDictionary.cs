﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  internal sealed class DictionaryScreen
  {
    internal DictionaryScreen(Application Application)
    {
      this.Screen = Application.NewScreen();
      Screen.Title = "Dictionary";
      Screen.Caption = "CLOSE";

      var Surface = Screen.Surface;

      this.WebBrowser = Surface.NewBrowser();
      Screen.Content = WebBrowser;
      //WebBrowser.Size.SetMaximum(710, 440);
    }

    public void Execute(string Word)
    {
      WebBrowser.LoadUri(new Uri("https://en.m.wiktionary.org/wiki/" + Word));

      Screen.Open();
    }

    private readonly Screen Screen;
    private readonly Inv.Browser WebBrowser;
  }
}