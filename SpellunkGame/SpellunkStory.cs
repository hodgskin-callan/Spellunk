﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  internal sealed class Story
  {
    public Story(Deck Deck)
    {
      this.Deck = Deck;
      this.SequenceList = new Inv.DistinctList<Sequence>();

      this.WordMatchSoundArray = new Inv.Sound[]
      {
        Resources.Sounds.WordMatch0,
        Resources.Sounds.WordMatch1,
        Resources.Sounds.WordMatch2,
        Resources.Sounds.WordMatch3
      };

      this.SwapMoveSound = Resources.Sounds.SwapMove;
      this.JumpMoveSound = Resources.Sounds.JumpMove;
      this.StampMoveSound = Resources.Sounds.StampMove;
    }

    public long Clock { get; private set; }

    public HashSet<Tile> GetAnimatingTileSet()
    {
      var Result = new HashSet<Tile>();

      foreach (var Sequence in GetSequences())
      {
        foreach (var Animation in Sequence.GetAnimations())
        {
          if (Animation.TileArray != null && Clock >= Animation.Start && Clock <= Animation.Finish)
            Result.AddRange(Animation.TileArray);
        }
      }

      return Result;
    }
    public bool IsAnimating()
    {
      return SequenceList.Count > 0;
    }
    public IEnumerable<Sequence> GetSequences()
    {
      return SequenceList;
    }
    public void AdvanceClock()
    {
      Clock++;
      SequenceList.RemoveAll(S => S.CompleteAnimations());
    }
    public void Fail(Slot Slot)
    {
      var Sequence = AddSequence();
      Sequence.AddAnimation(new Emitting(Resources.Sounds.Fail), 0, 1);
      Sequence.AddAnimation(new Failing(Slot), 0, 10);
    }
    public void Fail(Slot A, Slot B)
    {
      var Sequence = AddSequence();
      Sequence.AddAnimation(new Emitting(Resources.Sounds.Fail), 0, 1);
      Sequence.AddAnimation(new Failing(A), 0, 10);
      Sequence.AddAnimation(new Failing(B), 0, 10);
    }
    public void Swap(Slot A, Slot B)
    {
      if (Deck.Swaps <= 0 || !A.Tile.CanMove() || !B.Tile.CanMove())
      {
        Fail(A, B);
      }
      else
      {
        Deck.Swaps--;

        var Sequence = AddSequence();
        Sequence.AddAnimation(new Emitting(SwapMoveSound), 0, 1);
        Sequence.AddAnimation(new Swapping(A, B), 0, 20);
        Sequence.CompletedEvent += () =>
        {
          var SwapTile = A.Tile;
          A.Tile = B.Tile;
          B.Tile = SwapTile;
        };
      }
    }
    public void Jump(Slot A, Slot B)
    {
      if (Deck.Jumps <= 0 || !A.Tile.CanMove() || !B.Tile.CanMove())
      {
        Fail(A, B);
      }
      else
      {
        Deck.Jumps--;

        var Sequence = AddSequence();
        Sequence.AddAnimation(new Emitting(JumpMoveSound), 0, 1);
        Sequence.AddAnimation(new Swapping(A, B), 0, 20);
        Sequence.CompletedEvent += () =>
        {
          var SwapTile = A.Tile;
          A.Tile = B.Tile;
          B.Tile = SwapTile;
        };
      }
    }
    public void Stamp(Slot Slot, Symbol Symbol)
    {
      Debug.Assert(Slot.Tile.Symbol == null);

      if (Deck.Stamps <= 0)
      {
        Fail(Slot);
      }
      else
      {
        Deck.Stamps--;

        Slot.Tile.Symbol = Symbol;

        var Sequence = AddSequence();

        Sequence.AddAnimation(new Emitting(StampMoveSound), 0, 1);
        Sequence.AddAnimation(new Stamping(Slot), 0, 20);
      }
    }
    public void Match(Inv.DistinctList<Word> WordList)
    {
      var Sequence = AddSequence();

      var MatchDuration = 30;

      var MatchTotal = WordList.Count * MatchDuration;

      var WordIndex = 0;
      foreach (var Word in WordList)
      {
        var MatchOffset = WordIndex * MatchDuration;

        Sequence.AddAnimation(new Emitting(WordMatchSoundArray[WordIndex % WordMatchSoundArray.Length]), MatchOffset, 1);
        Sequence.AddAnimation(new Matching(Word.SlotList.ToArray()), MatchOffset, MatchTotal - MatchOffset);
        Sequence.AddAnimation(new Scoring(Word.Score, Word.SlotList.ToArray()), MatchOffset, MatchTotal - MatchOffset);

        WordIndex++;
      }

      Sequence.CompletedEvent += () =>
      {
        var Score = 0;

        foreach (var Word in WordList)
          Score += Word.Score;

        if (WordList.Count > 1)
          Score += 15; // multiple word bonus.

        if (WordList.Count > 0)
          Score += 5; // cascade bonus

        Deck.Words -= WordList.Count;

        var Move = Deck.AddMove();
        Move.Score = Score;
        Move.WordList.AddRange(WordList);

        BoostScore(Score);

        foreach (var Word in WordList)
        {
          foreach (var Slot in Word.SlotList)
            Slot.Tile = null;
        }

        Cascade();
      };
    }
    public void NextLevel(Action Action)
    {
      var Sequence = AddSequence();
      Sequence.AddAnimation(new Emitting(Resources.Sounds.AdvanceLevel), 0, 60);
      Sequence.CompletedEvent += Action;
    }
    public void Cascade()
    {
      var Sequence = AddSequence();

      for (var Column = 0; Column < Deck.Width; Column++)
      {
        for (var Row = Deck.Height - 1; Row >= 0; Row--)
        {
          var Slot = Deck[Column, Row];

          if (Slot.Tile == null)
          {
            var AboveSlot = Row == 0 ? null : Deck[Column, Row - 1];

            if (AboveSlot == null || (AboveSlot.Tile != null && AboveSlot.Tile.Style != Style.Broken))
            {
              if (AboveSlot != null)
              {
                Slot.Tile = AboveSlot.Tile;
                AboveSlot.Tile = null;
              }
              else
              {
                // new tile entering the board.
                Slot.Tile = Deck.Bag.GenerateTile();
              }

              Sequence.AddAnimation(new Falling(Slot), 0, 6);
            }
          }
        }
      }

      if (Sequence.HasAnimations())
      {
        Sequence.CompletedEvent += () =>
        {
          Cascade();
        };
      }
    }

    internal void BoostScore(int Score)
    {
      var OldRank = Deck.Score / 250;

      Deck.Score += Score;

      var NewRank = Deck.Score / 250;

      Deck.Swaps += (NewRank - OldRank) * 4; // 4 every 250.
      Deck.Stamps += (NewRank - OldRank) * 2; // 1 every 250.
      Deck.Jumps += ((NewRank / 2) - (OldRank / 2)); // 1 every 500

      if (Deck.Swaps > Rules.MaximumMoveCount)
        Deck.Swaps = Rules.MaximumMoveCount;

      if (Deck.Stamps > Rules.MaximumMoveCount)
        Deck.Stamps = Rules.MaximumMoveCount;

      if (Deck.Jumps > Rules.MaximumMoveCount)
        Deck.Jumps = Rules.MaximumMoveCount;
    }

    private Sequence AddSequence()
    {
      var Result = new Sequence(this);

      SequenceList.Add(Result);

      return Result;
    }

    private readonly Deck Deck;
    private readonly Inv.DistinctList<Sequence> SequenceList;
    private readonly Inv.Sound[] WordMatchSoundArray;
    private readonly Inv.Sound SwapMoveSound;
    private readonly Inv.Sound JumpMoveSound;
    private readonly Inv.Sound StampMoveSound;
  }

  internal sealed class Sequence
  {
    public Sequence(Story Story)
    {
      this.Story = Story;
      this.AnimationList = new Inv.DistinctList<Animation>();
    }

    public event Action CompletedEvent;

    public bool HasAnimations()
    {
      return AnimationList.Count > 0;
    }
    public IEnumerable<Animation> GetAnimations()
    {
      return AnimationList;
    }
    public void AddAnimation(Animation Animation, long Offset, long Duration)
    {
#if DEBUG
      //Offset *= 5; Duration *= 5; // slow down animations to debug them.
#endif

      Animation.Story = Story;
      Animation.Start = Story.Clock + Offset;
      Animation.Finish = Animation.Start + Duration - 1;
      AnimationList.Add(Animation);
    }
    public bool CompleteAnimations()
    {
      AnimationList.RemoveAll(A => Story.Clock > A.Finish);

      if (AnimationList.Count == 0)
      {
        if (CompletedEvent != null)
          CompletedEvent();

        return true;
      }

      return false;
    }

    private readonly Story Story;
    private readonly Inv.DistinctList<Animation> AnimationList;
  }

  internal abstract class Animation
  {
    public Story Story { get; internal set; }
    public long Start { get; internal set; }
    public long Finish { get; internal set; }
    public Tile[] TileArray { get; internal set; }

    public float GetProgress()
    {
      return (Finish - Story.Clock) / (float)(Finish - Start + 1);
    }
  }

  internal sealed class Swapping : Animation
  {
    internal Swapping(Slot A, Slot B)
    {
      this.A = A;
      this.B = B;
      this.TileArray = new[] { A.Tile, B.Tile };
    }

    public Slot A { get; private set; }
    public Slot B { get; private set; }
  }

  internal sealed class Falling : Animation
  {
    internal Falling(Slot Slot)
    {
      this.Slot = Slot;
      this.TileArray = new[] { Slot.Tile };
    }

    public Slot Slot { get; private set; }
  }

  internal sealed class Stamping : Animation
  {
    internal Stamping(Slot Slot)
    {
      this.Slot = Slot;
      this.TileArray = new[] { Slot.Tile };
    }

    public Slot Slot { get; private set; }
  }

  internal sealed class Failing : Animation
  {
    internal Failing(Slot Slot)
    {
      this.Slot = Slot;
      this.TileArray = new[] { Slot.Tile, Slot.Tile };
    }

    public Slot Slot { get; private set; }
  }

  internal sealed class Emitting : Animation
  {
    internal Emitting(Inv.Sound Sound)
    {
      this.Sound = Sound;
    }

    public Inv.Sound Sound { get; private set; }
    public bool Played { get; set; }
  }

  internal sealed class Matching : Animation
  {
    internal Matching(Slot[] SlotArray)
    {
      this.SlotArray = SlotArray;
      this.TileArray = SlotArray.Select(S => S.Tile).ToArray();
    }

    public Slot[] SlotArray { get; private set; }
  }

  internal sealed class Scoring : Animation
  {
    internal Scoring(int Score, Slot[] SlotArray)
    {
      this.Score = Score;
      this.SlotArray = SlotArray;
      this.TileArray = SlotArray.Select(S => S.Tile).ToArray();
    }

    public int Score { get; private set; }
    public Slot[] SlotArray { get; private set; }
  }
}
