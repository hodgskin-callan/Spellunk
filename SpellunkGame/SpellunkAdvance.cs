﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  internal sealed class AdvanceScreen
  {
    internal AdvanceScreen(Application Application)
    {
      this.Application = Application;
      this.Screen = Application.NewScreen();

      var Surface = Screen.Surface;

      var LayoutDock = Surface.NewVerticalDock();
      Screen.Content = LayoutDock;
      LayoutDock.Margin.Set(50);

      this.WordsLabel = Surface.NewLabel();
      LayoutDock.AddHeader(WordsLabel);
      WordsLabel.Margin.Set(10);
      WordsLabel.Font.Custom(20).SmallCaps();

      var StartingSwapsLabel = Surface.NewLabel();
      LayoutDock.AddHeader(StartingSwapsLabel);
      StartingSwapsLabel.Margin.Set(10);
      StartingSwapsLabel.Font.Custom(20).SmallCaps();
      StartingSwapsLabel.Text = "Moves available";

      this.SwapsLabel = Surface.NewLabel();
      LayoutDock.AddHeader(SwapsLabel);
      SwapsLabel.Alignment.StretchCenter();
      SwapsLabel.Font.Custom(20).SmallCaps().In(Inv.Colour.DarkRed);

      this.StampsLabel = Surface.NewLabel();
      LayoutDock.AddHeader(StampsLabel);
      StampsLabel.Alignment.StretchCenter();
      StampsLabel.Font.Custom(20).SmallCaps().In(Inv.Colour.DarkRed);

      this.JumpsLabel = Surface.NewLabel();
      LayoutDock.AddHeader(JumpsLabel);
      JumpsLabel.Alignment.StretchCenter();
      JumpsLabel.Font.Custom(20).SmallCaps().In(Inv.Colour.DarkRed);

      var BeginButton = Surface.NewFlatButton();
      LayoutDock.AddClient(BeginButton);
      BeginButton.Background.In(Inv.Colour.DarkGreen);
      BeginButton.Alignment.Center();
      BeginButton.Padding.Set(20);
      BeginButton.Elevation.Set(3);
      BeginButton.SingleTapEvent += () =>
      {
        Screen.Close();

        Application.BeginGame();
      };

      var BeginLabel = Surface.NewLabel();
      BeginButton.Content = BeginLabel;
      BeginLabel.Font.Custom(Theme.MassiveSize).In(Inv.Colour.White);
      BeginLabel.Text = "Next level!";
    }

    public void Execute()
    {
      Screen.Title = "Level " + Application.Deck.Level;

      WordsLabel.Text = Application.Deck.Words + " words to next level";
      SwapsLabel.Text = Application.Deck.Swaps + " swaps";
      StampsLabel.Text = Application.Deck.Stamps + " stamps";
      JumpsLabel.Text = Application.Deck.Jumps + " jumps";

      Screen.Open();
    }

    private readonly Application Application;
    private readonly Screen Screen;
    private readonly Inv.Label WordsLabel;
    private readonly Inv.Label SwapsLabel;
    private readonly Inv.Label StampsLabel;
    private readonly Inv.Label JumpsLabel;
  }
}
