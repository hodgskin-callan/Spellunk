﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  public static class Shell
  {
    public static readonly string AssetFolderPath = @"C:\Development\Spellunk\SpellunkGameW\Assets";

    public static void Install(Inv.Application Application)
    {
#if DEBUG
      Inv.Assert.Enable();
#else
      Inv.Assert.Disable();
#endif

      new Application(Application);
    }
  }
}
