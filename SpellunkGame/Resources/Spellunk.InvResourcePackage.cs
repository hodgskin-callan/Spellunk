#pragma warning disable 0649
namespace Spellunk
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.Spellunk.InvResourcePackage.rs");
    }

    public static readonly ResourcesCrystal Crystal;
    public static readonly ResourcesIcons Icons;
    public static readonly ResourcesSegoeUI SegoeUI;
    public static readonly ResourcesSounds Sounds;
    public static readonly ResourcesStompface Stompface;
    public static readonly ResourcesTexts Texts;
    public static readonly ResourcesTiles128 Tiles128;
    public static readonly ResourcesTiles256 Tiles256;
  }

  public sealed class ResourcesCrystal
  {
    public ResourcesCrystal() { }

    ///<Summary>(.png) 256 x 256 (115.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Broken;
    ///<Summary>(.png) 256 x 256 (64.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Golden;
    ///<Summary>(.png) 256 x 256 (18.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Match;
    ///<Summary>(.png) 256 x 256 (40.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Molten;
    ///<Summary>(.png) 256 x 256 (58.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Platinum;
    ///<Summary>(.png) 256 x 256 (18.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier1;
    ///<Summary>(.png) 256 x 256 (27.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier2;
    ///<Summary>(.png) 256 x 256 (22.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier3;
    ///<Summary>(.png) 256 x 256 (51.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier4;
    ///<Summary>(.png) 256 x 256 (57.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Wooden;
  }

  public sealed class ResourcesIcons
  {
    public ResourcesIcons() { }

    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Help;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Menu;
    ///<Summary>(.png) 256 x 256 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SoundOff;
    ///<Summary>(.png) 256 x 256 (3.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SoundOn;
  }

  public sealed class ResourcesSegoeUI
  {
    public ResourcesSegoeUI() { }

    ///<Summary>(.png) 256 x 256 (3.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference A;
    ///<Summary>(.png) 256 x 256 (3.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference B;
    ///<Summary>(.png) 256 x 256 (3.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference C;
    ///<Summary>(.png) 256 x 256 (3.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference D;
    ///<Summary>(.png) 256 x 256 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference E;
    ///<Summary>(.png) 256 x 256 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference F;
    ///<Summary>(.png) 256 x 256 (4.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference G;
    ///<Summary>(.png) 256 x 256 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference H;
    ///<Summary>(.png) 256 x 256 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference I;
    ///<Summary>(.png) 256 x 256 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference J;
    ///<Summary>(.png) 256 x 256 (3.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference K;
    ///<Summary>(.png) 256 x 256 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference L;
    ///<Summary>(.png) 256 x 256 (3.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference M;
    ///<Summary>(.png) 256 x 256 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference N;
    ///<Summary>(.png) 256 x 256 (4.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference O;
    ///<Summary>(.png) 256 x 256 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference P;
    ///<Summary>(.txt) In Gimp (0.3KB)</Summary>
    public readonly global::Inv.Resource.TextReference Procedure;
    ///<Summary>(.png) 256 x 256 (5.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Q;
    ///<Summary>(.png) 256 x 256 (3.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference R;
    ///<Summary>(.png) 256 x 256 (3.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference S;
    ///<Summary>(.png) 256 x 256 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference T;
    ///<Summary>(.png) 256 x 256 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference U;
    ///<Summary>(.png) 256 x 256 (3.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference V;
    ///<Summary>(.png) 256 x 256 (4.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference W;
    ///<Summary>(.png) 256 x 256 (4.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference X;
    ///<Summary>(.png) 256 x 256 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Y;
    ///<Summary>(.png) 256 x 256 (2.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Z;
  }

  public sealed class ResourcesSounds
  {
    public ResourcesSounds() { }

    ///<Summary>(.mp3) ~ (39.9KB)</Summary>
    public readonly global::Inv.Resource.SoundReference AdvanceLevel;
    ///<Summary>(.mp3) ~ (1.9KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Click;
    ///<Summary>(.mp3) ~ (45.4KB)</Summary>
    public readonly global::Inv.Resource.SoundReference EndGame;
    ///<Summary>(.mp3) ~ (6.2KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Fail;
    ///<Summary>(.mp3) ~ (11.5KB)</Summary>
    public readonly global::Inv.Resource.SoundReference JumpMove;
    ///<Summary>(.mp3) ~ (10.4KB)</Summary>
    public readonly global::Inv.Resource.SoundReference StampMove;
    ///<Summary>(.mp3) ~ (3.0KB)</Summary>
    public readonly global::Inv.Resource.SoundReference SwapMove;
    ///<Summary>(.mp3) ~ (10.1KB)</Summary>
    public readonly global::Inv.Resource.SoundReference WordMatch0;
    ///<Summary>(.mp3) ~ (9.2KB)</Summary>
    public readonly global::Inv.Resource.SoundReference WordMatch1;
    ///<Summary>(.mp3) ~ (9.3KB)</Summary>
    public readonly global::Inv.Resource.SoundReference WordMatch2;
    ///<Summary>(.mp3) ~ (10.2KB)</Summary>
    public readonly global::Inv.Resource.SoundReference WordMatch3;
  }

  public sealed class ResourcesStompface
  {
    public ResourcesStompface() { }

    ///<Summary>(.png) 256 x 256 (3.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference A;
    ///<Summary>(.png) 256 x 256 (3.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference B;
    ///<Summary>(.png) 256 x 256 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference C;
    ///<Summary>(.png) 256 x 256 (3.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference D;
    ///<Summary>(.png) 256 x 256 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference E;
    ///<Summary>(.png) 256 x 256 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference F;
    ///<Summary>(.png) 256 x 256 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference G;
    ///<Summary>(.png) 256 x 256 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference H;
    ///<Summary>(.png) 256 x 256 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference I;
    ///<Summary>(.png) 256 x 256 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference J;
    ///<Summary>(.png) 256 x 256 (3.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference K;
    ///<Summary>(.png) 256 x 256 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference L;
    ///<Summary>(.png) 256 x 256 (3.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference M;
    ///<Summary>(.png) 256 x 256 (3.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference N;
    ///<Summary>(.png) 256 x 256 (3.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference O;
    ///<Summary>(.png) 256 x 256 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference P;
    ///<Summary>(.png) 256 x 256 (3.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Q;
    ///<Summary>(.png) 256 x 256 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference R;
    ///<Summary>(.png) 256 x 256 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference S;
    ///<Summary>(.png) 256 x 256 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference T;
    ///<Summary>(.png) 256 x 256 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference U;
    ///<Summary>(.png) 256 x 256 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference V;
    ///<Summary>(.png) 256 x 256 (3.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference W;
    ///<Summary>(.png) 256 x 256 (3.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference X;
    ///<Summary>(.png) 256 x 256 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Y;
    ///<Summary>(.png) 256 x 256 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Z;
  }

  public sealed class ResourcesTexts
  {
    public ResourcesTexts() { }

    ///<Summary>(.txt) a2023.1109.0808 (0.0KB)</Summary>
    public readonly global::Inv.Resource.TextReference Version;
  }

  public sealed class ResourcesTiles128
  {
    public ResourcesTiles128() { }

    ///<Summary>(.png) 128 x 128 (34.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Broken;
    ///<Summary>(.png) 128 x 128 (19.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Golden;
    ///<Summary>(.png) 128 x 128 (6.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Match;
    ///<Summary>(.png) 128 x 128 (14.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Molten;
    ///<Summary>(.png) 128 x 128 (14.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Platinum;
    ///<Summary>(.png) 128 x 128 (6.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier1;
    ///<Summary>(.png) 128 x 128 (9.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier2;
    ///<Summary>(.png) 128 x 128 (7.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier3;
    ///<Summary>(.png) 128 x 128 (16.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier4;
    ///<Summary>(.png) 128 x 128 (18.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Wooden;
  }

  public sealed class ResourcesTiles256
  {
    public ResourcesTiles256() { }

    ///<Summary>(.png) 256 x 256 (115.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Broken;
    ///<Summary>(.png) 256 x 256 (60.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Golden;
    ///<Summary>(.png) 256 x 256 (18.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Match;
    ///<Summary>(.png) 256 x 256 (40.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Molten;
    ///<Summary>(.png) 256 x 256 (43.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Platinum;
    ///<Summary>(.png) 256 x 256 (18.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier1;
    ///<Summary>(.png) 256 x 256 (27.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier2;
    ///<Summary>(.png) 256 x 256 (22.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier3;
    ///<Summary>(.png) 256 x 256 (51.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tier4;
    ///<Summary>(.png) 256 x 256 (57.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Wooden;
  }
}