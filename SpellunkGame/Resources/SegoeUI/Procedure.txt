In Gimp

Use the Text widget
Type the letter
Change to Segoe UI, 128pt, Black
Image > Autocrop Image
Canvas Size, set to 256x256 and centered
Filters > Light and Shadow > Drop Shadow
Select offset of 0,0 and blur radius of 8, black with opacity 50% and don't allow resizing of the image