﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  internal sealed class LadderScreen
  {
    internal LadderScreen(Application Application)
    {
      this.Screen = Application.NewScreen();
      Screen.Caption = "CLOSE";
      Screen.CloseEvent += () =>
      {
        if (IsEnded)
        {
          // update the ladder if you made the high score.
          if (Ladder.RecordList.Contains(NewRecord))
            Application.SaveLadder(Ladder);

          Application.NewGame();

          this.Ladder = null;
          this.NewRecord = null;
          this.IsEnded = false;
        }
      };

      this.Surface = Screen.Surface;

      var LayoutDock = Surface.NewVerticalDock();
      Screen.Content = LayoutDock;
      LayoutDock.Alignment.Center();

      this.SummaryLabel = Surface.NewLabel();
      LayoutDock.AddHeader(SummaryLabel);
      SummaryLabel.Margin.Set(0, 20, 0, 0);
      SummaryLabel.Font.Custom(20).SmallCaps();

      this.LadderLabel = Surface.NewLabel();
      LayoutDock.AddHeader(LadderLabel);
      LadderLabel.Margin.Set(0, 5, 0, 20);
      LadderLabel.Font.Custom(20).SmallCaps();

      var LadderScroll = Surface.NewVerticalScroll();
      LayoutDock.AddClient(LadderScroll);

      this.LadderStack = Surface.NewVerticalStack();
      LadderScroll.Content = LadderStack;
      LadderStack.Margin.Set(0, 0, 0, 20);
    }

    public void Execute(Record NewRecord, Ladder Ladder)
    {
      this.Ladder = Ladder;
      this.NewRecord = NewRecord;
      this.IsEnded = NewRecord != null;

      Screen.Title = IsEnded ? "Game Over" : "Ladder";

      SummaryLabel.Text = IsEnded ? string.Format("Game ended on level {0} with score of {1} after {2} moves and {3} words formed.", NewRecord.Level, NewRecord.Score, NewRecord.Moves, NewRecord.Words) : "";

      if (!IsEnded)
      {
        LadderLabel.Text = "";
        LadderLabel.Font.In(Inv.Colour.Blue);
      }
      else if (Ladder.RecordList.Contains(NewRecord))
      {
        LadderLabel.Text = "Your score made the ladder!";
        LadderLabel.Font.In(Inv.Colour.Blue);
      }
      else
      {
        LadderLabel.Text = "Your score did not make the ladder.";
        LadderLabel.Font.In(Inv.Colour.DarkBlue);
      }

      LadderStack.RemovePanels();

      foreach (var Record in Ladder.RecordList)
      {
        var RecordFrame = Surface.NewFrame();
        LadderStack.AddPanel(RecordFrame);
        RecordFrame.Padding.Set(5);
        RecordFrame.Border.Set(0, 0, 0, 1);

        var RecordDock = Surface.NewHorizontalDock();
        RecordFrame.Content = RecordDock;

        var ScoreLabel = Surface.NewLabel();
        RecordDock.AddHeader(ScoreLabel);
        ScoreLabel.Text = Record.Score.ToString();
        ScoreLabel.Size.SetWidth(100);
        ScoreLabel.Font.Custom(16).In(Inv.Colour.Purple);

        var LevelLabel = Surface.NewLabel();
        RecordDock.AddFooter(LevelLabel);
        LevelLabel.Text = "lvl " + Record.Level.ToString();
        LevelLabel.Size.SetWidth(100);
        LevelLabel.Justify.Right();
        LevelLabel.Font.SmallCaps().In(Inv.Colour.DarkRed);

        if (Record == NewRecord)
        {
          RecordFrame.Border.In(Inv.Colour.DarkRed);
          RecordFrame.Background.In(Inv.Colour.AliceBlue);

          var NameEdit = Surface.NewNameEdit();
          RecordDock.AddClient(NameEdit);
          NameEdit.Text = Record.Name;
          NameEdit.Font.Custom(16);
          NameEdit.ChangeEvent += () => NewRecord.Name = NameEdit.Text;
          //NameEdit.MaximumLength = 20;
          NameEdit.Focus.Set();
        }
        else
        {
          RecordFrame.Border.In(Inv.Colour.DimGray);

          var NameLabel = Surface.NewLabel();
          RecordDock.AddClient(NameLabel);
          NameLabel.Text = Record.Name;
          NameLabel.Alignment.StretchLeft();
          NameLabel.Font.Custom(16);
        }
      }

      Screen.Open();
    }

    private readonly Inv.Surface Surface;
    private readonly Screen Screen;
    private readonly Inv.Stack LadderStack;
    private readonly Inv.Label SummaryLabel;
    private readonly Inv.Label LadderLabel;
    private Ladder Ladder;
    private Record NewRecord;
    private bool IsEnded;
  }
}
