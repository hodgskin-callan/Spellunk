﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Inv.Support;

namespace Spellunk
{
  internal static class Syntax
  {
    static Syntax()
    {
      // Load the keywords from the reflected constant fields.
      var SyntaxGrammarType = typeof(Syntax);

      foreach (var FieldInfo in SyntaxGrammarType.GetReflectionFields())
      {
        if (FieldInfo.Name.EndsWith("Keyword") && FieldInfo.FieldType == typeof(string))
          Grammar.Keyword.Add((string)FieldInfo.GetValue(SyntaxGrammarType));
      }
    }

    public static void SaveDeck(Deck Deck, Inv.File File)
    {
      File.AsSyntax(Grammar).Write(SyntaxWriter =>
      {
        SyntaxWriter.WriteKeyword(Syntax.StartKeyword);
        SyntaxWriter.WriteSpace();
        SyntaxWriter.WriteTimestamp(Deck.StartTime);
        SyntaxWriter.WriteLine();
        SyntaxWriter.WriteKeyword(Syntax.ScoreKeyword);
        SyntaxWriter.WriteSpace();
        SyntaxWriter.WriteInteger(Deck.Score);
        SyntaxWriter.WriteLine();
        SyntaxWriter.WriteKeyword(Syntax.LevelKeyword);
        SyntaxWriter.WriteSpace();
        SyntaxWriter.WriteInteger(Deck.Level);
        SyntaxWriter.WriteSpace();
        SyntaxWriter.WritePipe();
        SyntaxWriter.WriteSpace();
        SyntaxWriter.WriteInteger(Deck.Words);
        SyntaxWriter.WriteLine();

        SyntaxWriter.WriteLine();
        SyntaxWriter.WriteOpenScopeKeyword(Syntax.MovesKeyword);

        SaveMoveCount(SyntaxWriter, Syntax.SwapsKeyword, Deck.Swaps);
        SaveMoveCount(SyntaxWriter, Syntax.StampsKeyword, Deck.Stamps);
        SaveMoveCount(SyntaxWriter, Syntax.JumpsKeyword, Deck.Jumps);

        if (Deck.GetMoves().Any())
        {
          SyntaxWriter.WriteLine();

          foreach (var Move in Deck.GetMoves())
          {
            SyntaxWriter.WriteIndent();
            SyntaxWriter.WriteTimestamp(Move.Time);
            SyntaxWriter.WriteSpace();
            SyntaxWriter.WriteInteger(Move.Score);
            SyntaxWriter.WriteColon();
            SyntaxWriter.WriteSpace();

            foreach (var Word in Move.WordList)
            {
              SyntaxWriter.WriteString(Word.Text);
              SyntaxWriter.WriteSpace();
              SyntaxWriter.WriteInteger(Word.Score);

              if (Word != Move.WordList.Last())
              {
                SyntaxWriter.WriteComma();
                SyntaxWriter.WriteSpace();
              }
            }

            SyntaxWriter.WriteSemicolon();
            SyntaxWriter.WriteLine();
          }
        }

        SyntaxWriter.WriteCloseScope();

        SyntaxWriter.WriteLineAndIndent();
        SyntaxWriter.WriteKeyword(Syntax.SquaresKeyword);
        SyntaxWriter.WriteSpace();
        SyntaxWriter.WriteInteger(Deck.Width);
        SyntaxWriter.WriteSpace();
        SyntaxWriter.WriteIdentifier("x");
        SyntaxWriter.WriteSpace();
        SyntaxWriter.WriteInteger(Deck.Height);
        SyntaxWriter.WriteLine();
        SyntaxWriter.WriteOpenScope();

        foreach (var Slot in Deck.GetSlots())
        {
          SyntaxWriter.WriteIndent();
          SyntaxWriter.WriteOpenRound();
          SyntaxWriter.WriteInteger(Slot.X);
          SyntaxWriter.WriteComma();
          SyntaxWriter.WriteSpace();
          SyntaxWriter.WriteInteger(Slot.Y);
          SyntaxWriter.WriteCloseRound();

          if (Slot.Tile != null)
          {
            SyntaxWriter.WriteColon();
            SyntaxWriter.WriteSpace();
            if (Slot.Tile.Symbol != null)
              SyntaxWriter.WriteIdentifier(Slot.Tile.Symbol.Letter.ToString());
            else
              SyntaxWriter.WriteQuestionMark();

            SyntaxWriter.WriteSpace();
            SyntaxWriter.WriteIdentifier(Slot.Tile.Style.ToString().ToLower());
          }

          SyntaxWriter.WriteSemicolon();
          SyntaxWriter.WriteLine();
        }

        SyntaxWriter.WriteCloseScope();
      });
    }
    public static void LoadDeck(Deck Deck, Inv.File File)
    {
      File.AsSyntax(Grammar).Read(SyntaxReader =>
      {
        Deck.Clear();

        if (SyntaxReader.ReadOptionalKeywordValue(Syntax.StartKeyword))
          Deck.StartTime = SyntaxReader.ReadTimestamp();
        else
          Deck.StartTime = DateTimeOffset.Now;

        SyntaxReader.ReadKeywordValue(Syntax.ScoreKeyword);
        Deck.Score = SyntaxReader.ReadInteger32();

        SyntaxReader.ReadKeywordValue(Syntax.LevelKeyword);
        Deck.Level = SyntaxReader.ReadInteger32();
        SyntaxReader.ReadPipe();
        Deck.Words = SyntaxReader.ReadInteger32();

        SyntaxReader.ReadKeywordValue(Syntax.MovesKeyword);
        SyntaxReader.ReadOpenScope();

        Deck.Swaps = LoadMoveCount(SyntaxReader, Syntax.SwapsKeyword);
        Deck.Stamps = LoadMoveCount(SyntaxReader, Syntax.StampsKeyword);
        Deck.Jumps = LoadMoveCount(SyntaxReader, Syntax.JumpsKeyword);

        while (!SyntaxReader.NextIsTokenType(Inv.Syntax.TokenType.CloseBrace))
        {
          var Move = Deck.AddMove();
          Move.Time = SyntaxReader.ReadTimestamp();
          Move.Score = SyntaxReader.ReadInteger32();
          SyntaxReader.ReadColon();

          do
          {
            var Word = new Word();
            Move.WordList.Add(Word);
            Word.Text = SyntaxReader.ReadString();
            Word.Score = SyntaxReader.ReadInteger32();
          }
          while (SyntaxReader.ReadOptionalComma());

          SyntaxReader.ReadSemicolon();
        }

        SyntaxReader.ReadCloseScope();

        SyntaxReader.ReadKeywordValue(Syntax.SquaresKeyword);

        if (SyntaxReader.ReadInteger32() != Deck.Width)
          throw new Exception("Unexpected width.");

        SyntaxReader.ReadIdentifierValue("x");

        if (SyntaxReader.ReadInteger32() != Deck.Height)
          throw new Exception("Unexpected height.");

        SyntaxReader.ReadOpenScope();

        while (!SyntaxReader.NextIsTokenType(Inv.Syntax.TokenType.CloseBrace))
        {
          SyntaxReader.ReadOpenRound();
          var X = SyntaxReader.ReadInteger32();
          SyntaxReader.ReadComma();
          var Y = SyntaxReader.ReadInteger32();
          SyntaxReader.ReadCloseRound();

          var Slot = Deck[X, Y];

          if (SyntaxReader.ReadOptionalColon())
          {
            Slot.Tile = new Tile();

            if (SyntaxReader.ReadOptionalQuestionMark())
              Slot.Tile.Symbol = null;
            else
              Slot.Tile.Symbol = Deck.Bag.SymbolByLetter(SyntaxReader.ReadIdentifier().Single());

            Slot.Tile.Style = EnumHelper.Parse<Style>(SyntaxReader.ReadIdentifier(), true);
          }
          else
          {
            Slot.Tile = null;
          }

          SyntaxReader.ReadSemicolon();
        }

        SyntaxReader.ReadCloseScope();
      });
    }
    public static void LoadLadder(Ladder Ladder, Inv.File File)
    {
      File.AsSyntax(Syntax.Grammar).Read(SyntaxReader =>
      {
        SyntaxReader.ReadOpenScopeKeyword(Syntax.LadderKeyword);

        while (!SyntaxReader.NextIsTokenType(Inv.Syntax.TokenType.CloseBrace))
        {
          var Record = new Record();
          Ladder.RecordList.Add(Record);

          ReadRecord(SyntaxReader, Record);
        }

        SyntaxReader.ReadCloseScope();
      });

      // ensure the records are sorted in descending order by score.
      Ladder.RecordList.Sort((A, B) => B.Score.CompareTo(A.Score));
    }
    public static void SaveLadder(Ladder Ladder, Inv.File File)
    {
      File.AsSyntax(Syntax.Grammar).Write(SyntaxWriter =>
      {
        SyntaxWriter.WriteOpenScopeKeyword(Syntax.LadderKeyword);

        foreach (var Record in Ladder.RecordList)
        {
          SyntaxWriter.WriteIndent();

          WriteRecord(SyntaxWriter, Record);

          SyntaxWriter.WriteLine();
        }

        SyntaxWriter.WriteCloseScope();
      });
    }
    public static void SaveRecord(Record Record, Inv.File File)
    {
      File.AsSyntax(Syntax.Grammar).Write(SyntaxWriter =>
      {
        SyntaxWriter.WriteLine();
        WriteRecord(SyntaxWriter, Record);
      });
    }

    private static void WriteRecord(Inv.Syntax.Writer SyntaxWriter, Record Record)
    {
      SyntaxWriter.WriteTimestamp(Record.BeginTime);
      SyntaxWriter.WriteSpace();
      SyntaxWriter.WriteMinus();
      SyntaxWriter.WriteSpace();
      SyntaxWriter.WriteTimestamp(Record.EndTime);
      SyntaxWriter.WriteComma();
      SyntaxWriter.WriteSpace();

      SyntaxWriter.WriteString(Record.Name);
      SyntaxWriter.WriteComma();
      SyntaxWriter.WriteSpace();

      SyntaxWriter.WriteKeyword(Syntax.LevelKeyword);
      SyntaxWriter.WriteSpace();
      SyntaxWriter.WriteInteger(Record.Level);
      SyntaxWriter.WriteComma();
      SyntaxWriter.WriteSpace();

      SyntaxWriter.WriteKeyword(Syntax.ScoreKeyword);
      SyntaxWriter.WriteSpace();
      SyntaxWriter.WriteInteger(Record.Score);
      SyntaxWriter.WriteComma();
      SyntaxWriter.WriteSpace();

      SyntaxWriter.WriteKeyword(Syntax.WordsKeyword);
      SyntaxWriter.WriteSpace();
      SyntaxWriter.WriteInteger(Record.Words);
      SyntaxWriter.WriteComma();
      SyntaxWriter.WriteSpace();

      SyntaxWriter.WriteKeyword(Syntax.MovesKeyword);
      SyntaxWriter.WriteSpace();
      SyntaxWriter.WriteInteger(Record.Moves);
    }
    private static void ReadRecord(Inv.Syntax.Reader SyntaxReader, Record Record)
    {
      Record.BeginTime = SyntaxReader.ReadTimestamp();
      SyntaxReader.ReadMinus();
      Record.EndTime = SyntaxReader.ReadTimestamp();
      SyntaxReader.ReadComma();

      Record.Name = SyntaxReader.ReadString();
      SyntaxReader.ReadComma();

      SyntaxReader.ReadKeywordValue(Syntax.LevelKeyword);
      Record.Level = SyntaxReader.ReadInteger32();
      SyntaxReader.ReadComma();

      SyntaxReader.ReadKeywordValue(Syntax.ScoreKeyword);
      Record.Score = SyntaxReader.ReadInteger32();
      SyntaxReader.ReadComma();

      SyntaxReader.ReadKeywordValue(Syntax.WordsKeyword);
      Record.Words = SyntaxReader.ReadInteger32();
      SyntaxReader.ReadComma();

      SyntaxReader.ReadKeywordValue(Syntax.MovesKeyword);
      Record.Moves = SyntaxReader.ReadInteger32();
    }
    private static int LoadMoveCount(Inv.Syntax.Reader SyntaxReader, string Keyword)
    {
      SyntaxReader.ReadKeywordValue(Keyword);
      SyntaxReader.ReadEqualSign();
      var Result = SyntaxReader.ReadInteger32();
      SyntaxReader.ReadSemicolon();

      return Result;
    }
    private static void SaveMoveCount(Inv.Syntax.Writer SyntaxWriter, string Keyword, int Number)
    {
      SyntaxWriter.WriteIndent();
      SyntaxWriter.WriteKeyword(Keyword);
      SyntaxWriter.WriteSpace();
      SyntaxWriter.WriteEqualSign();
      SyntaxWriter.WriteSpace();
      SyntaxWriter.WriteInteger(Number);
      SyntaxWriter.WriteSemicolon();
      SyntaxWriter.WriteLine();
    }

    private static readonly Inv.Syntax.Grammar Grammar = new Inv.Syntax.Grammar()
    {
      IgnoreComment = true,
      IgnoreWhitespace = true,
      AllowMultiLineString = false,
      KeywordCaseSensitive = true,
      SingleLineCommentPrefix = "//",
      MultiLineCommentStart = "/*",
      MultiLineCommentFinish = "*/",
      StringAroundQuote = '"',
      StringEscape = '\\',
      StringVerbatim = '@',
      IdentifierOpenQuote = '[',
      IdentifierCloseQuote = ']',
      DateTimePrefix = "dt ",
      TimeSpanPrefix = "ts ",
      UseHexadecimalPrefix = false,
      UseBase64Prefix = true
    };

    private const string LadderKeyword = "ladder";
    private const string LevelKeyword = "level";
    private const string JumpsKeyword = "jumps";
    private const string MovesKeyword = "moves";
    private const string ScoreKeyword = "score";
    private const string SquaresKeyword = "squares";
    private const string StampsKeyword = "stamps";
    private const string StartKeyword = "start";
    private const string SwapsKeyword = "swaps";
    private const string WordsKeyword = "words";
  }
}