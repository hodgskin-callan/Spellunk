﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using Inv.Support;

namespace Spellunk
{
  internal sealed class Rules
  {
    internal const int MinimumWordLength = 4;
    internal const int MaximumWordLength = 8;
    internal const int MaximumMoveCount = 99;
  }

  internal sealed class Deck
  {
    public Deck(Bag Bag)
    {
      this.Bag = Bag;

      this.SlotGrid = new Inv.Grid<Slot>(8, 8);
      SlotGrid.Fill((X, Y) => new Slot(X, Y));

      this.MoveList = new Inv.DistinctList<Move>();
    }

    public Bag Bag { get; private set; }
    public DateTimeOffset StartTime { get; set; }
    public int Level { get; set; }
    public int Words { get; set; }
    public int Score { get; set; }
    public int Swaps { get; set; }
    public int Jumps { get; set; }
    public int Stamps { get; set; }
    public int Width => SlotGrid.Width;
    public int Height => SlotGrid.Height;
    public Slot this[int X, int Y] => SlotGrid[X, Y];

    public bool IsValid(int X, int Y)
    {
      return SlotGrid.IsValid(X, Y);
    }
    public IEnumerable<Slot> GetSlots()
    {
      return SlotGrid;
    }
    public Move AddMove()
    {
      var Result = new Move();

      MoveList.Add(Result);

      return Result;
    }
    public void ClearMoves()
    {
      MoveList.Clear();
    }
    public IEnumerable<Move> GetMoves()
    {
      return MoveList;
    }
    public void Clear()
    {
      foreach (var Slot in GetSlots())
        Slot.Tile = null;

      Level = 0;
      Words = 0;
      Score = 0;
      Swaps = 0;
      Jumps = 0;
      Stamps = 0;
      MoveList.Clear();
    }

    private readonly Inv.Grid<Slot> SlotGrid;
    private readonly Inv.DistinctList<Move> MoveList;
  }
    
  internal sealed class Slot
  {
    internal Slot(int X, int Y)
    {
      this.X = X;
      this.Y = Y;
    }

    public readonly int X;
    public readonly int Y;
    public Tile Tile { get; set; }

    public bool IsIdentical(Slot Slot)
    {
      return this.Tile.Symbol == Slot.Tile.Symbol && this.Tile.Style == Slot.Tile.Style;
    }
    public bool IsApart(Slot Slot)
    {
      return (Math.Abs(this.Y - Slot.Y) > 1 || Math.Abs(this.X - Slot.X) > 1);
    }
    public bool IsAdjacent(Slot Slot)
    {
      return (this.X == Slot.X && Math.Abs(this.Y - Slot.Y) == 1) || (Math.Abs(this.X - Slot.X) == 1 && this.Y == Slot.Y);
    }
  }

  internal sealed class Tile
  {
    public Symbol Symbol { get; set; }
    public Style Style { get; set; }

    public bool CanMove()
    {
      return Style != Style.Broken && Style != Style.Molten;
    }
    public Inv.Image GetImage()
    {
      Inv.Image Image;

      var Tiles = Theme.Tiles;

      switch (Style)
      {
        case Style.Wooden:
          Image = Tiles.Wooden;
          break;

        case Style.Broken:
          Image = Tiles.Broken;
          break;

        case Style.Molten:
          Image = Tiles.Molten;
          break;

        case Style.Silver:
          var Tier = Symbol.Tier;

          if (Tier.Index == 0)
            Image = Tiles.Tier1;
          else if (Tier.Index == 1)
            Image = Tiles.Tier2;
          else if (Tier.Index == 2)
            Image = Tiles.Tier3;
          else if (Tier.Index == 3)
            Image = Tiles.Tier4;
          else
            throw new Exception("Tier was not handled.");
          break;

        case Style.Gold:
          Image = Tiles.Golden;
          break;

        case Style.Platinum:
          Image = Tiles.Platinum;
          break;

        default:
          throw new Exception("Style not handled: " + Style);
      }

      return Image;
    }
  }

  internal enum Style
  {
    Silver, Gold, Platinum, Wooden, Molten, Broken
  }

  internal sealed class Move
  {
    internal Move()
    {
      this.WordList = new Inv.DistinctList<Word>();
    }

    public DateTimeOffset Time { get; set; }
    public int Score { get; set; }
    public Inv.DistinctList<Word> WordList { get; }
  }

  internal sealed class Word
  {
    public Word()
    {
      this.SlotList = new Inv.DistinctList<Slot>();
    }

    public string Text { get; set; }
    public int Score { get; set; }
    public Inv.DistinctList<Slot> SlotList { get; set; }
  }

  internal sealed class Ladder
  {
    public Ladder()
    {
      this.RecordList = new Inv.DistinctList<Record>();
    }

    public Inv.DistinctList<Record> RecordList { get; set; }
  }

  internal sealed class Record
  {
    public string Name { get; set; }
    public DateTimeOffset BeginTime { get; set; }
    public DateTimeOffset EndTime { get; set; }
    public int Level { get; set; }
    public int Score { get; set; }
    public int Moves { get; set; }
    public int Words { get; set; }
  }

  internal sealed class Bag
  {
    internal Bag()
    {
      this.TierList = new Inv.DistinctList<Tier>();
      this.LetterSymbolDictionary = new Dictionary<char, Symbol>();

      this.Tier1 = AddTier(1);
      Tier1.AddSymbol(2, 'A');
      Tier1.AddSymbol(3, 'E');
      Tier1.AddSymbol(2, 'I');
      Tier1.AddSymbol(2, 'O');
      Tier1.AddSymbol(1, 'U');

      this.Tier2 = AddTier(2);
      Tier2.AddSymbol(1, 'D', 'N', 'L', 'R', 'S', 'T');

      this.Tier3 = AddTier(4);
      Tier3.AddSymbol(1, 'B', 'C', 'F', 'G', 'H', 'K', 'M', 'P', 'W', 'Y');

      this.Tier4 = AddTier(8);
      Tier4.AddSymbol(1, 'J', 'Q', 'V', 'X', 'Z');

      foreach (var Tier in TierList)
      {
        Tier.SymbolDistributionArray = new Symbol[Tier.SymbolList.Sum(Index => Index.Weight)];

        var WeightCount = 0;
        foreach (var Symbol in Tier.SymbolList)
        {
          for (var WeightIndex = 0; WeightIndex < Symbol.Weight; WeightIndex++)
            Tier.SymbolDistributionArray[WeightCount++] = Symbol;

          LetterSymbolDictionary.Add(Symbol.Letter, Symbol);
        }
      }

      // Tile tier distribution.
      var TierQuantity = new Dictionary<Tier, int>
      {
        { Tier1, 42 },
        { Tier2, 32 },
        { Tier3, 21 },
        { Tier4, 5 },
      };

      this.TierDistributionArray = new Tier[TierQuantity.Values.Sum()];

      Debug.Assert(TierDistributionArray.Length == 100, "Expected a tier balance of 100.");

      var TierCount = 0;
      foreach (var TierEntry in TierQuantity)
      {
        for (var TierIndex = 0; TierIndex < TierEntry.Value; TierIndex++)
          TierDistributionArray[TierCount++] = TierEntry.Key;
      }

      // Tile style distribution.
      var TileBag = new Inv.EnumArray<Style, int>
      {
        { Style.Wooden, 35 },
        { Style.Silver, 150 },
        { Style.Gold, 4 },
        { Style.Platinum, 1 },
        { Style.Molten, 5 },
        { Style.Broken, 5 },
      };

      this.TileDistributionArray = new Style[TileBag.Sum()];

      Debug.Assert(TileDistributionArray.Length == 200, "Expected a style balance of 200.");

      var TileCount = 0;
      foreach (var TileEntry in TileBag.GetTuples())
      {
        for (var TileIndex = 0; TileIndex < TileEntry.Value; TileIndex++)
          TileDistributionArray[TileCount++] = TileEntry.Key;
      }

      this.Random = new Random();
    }

    public Inv.DistinctList<Tier> TierList { get; }
    public Tier Tier1 { get; }
    public Tier Tier2 { get; }
    public Tier Tier3 { get; }
    public Tier Tier4 { get; }

    public IEnumerable<Symbol> GetSymbols()
    {
      return LetterSymbolDictionary.Values.OrderBy(S => S.Letter);
    }
    public Symbol SymbolByLetter(char Letter)
    {
      return LetterSymbolDictionary[Letter];
    }
    public Tier TierByLetter(char Letter)
    {
      return SymbolByLetter(Letter).Tier;
    }
    public Tile GenerateTile()
    {
      var Style = Random.NextItem(TileDistributionArray);

      Symbol Symbol;

      if (Style == Style.Wooden)
      {
        Symbol = null;
      }
      else
      {
        var SymbolTier = Random.NextItem(TierDistributionArray);

        Symbol = Random.NextItem(SymbolTier.SymbolDistributionArray);
      }

      return new Tile()
      {
        Symbol = Symbol,
        Style = Style
      };
    }

    private Tier AddTier(int Score)
    {
      var Tier = new Tier(TierList.Count, Score);

      TierList.Add(Tier);

      return Tier;
    }

    private readonly Dictionary<char, Symbol> LetterSymbolDictionary;
    private readonly Tier[] TierDistributionArray;
    private readonly Style[] TileDistributionArray;
    private readonly Random Random;
  }

  internal sealed class Tier
  {
    internal Tier(int Index, int Score)
    {
      this.Index = Index;
      this.Score = Score;
      this.SymbolList = new Inv.DistinctList<Symbol>();
    }

    public void AddSymbol(int Weight, params char[] LetterArray)
    {
      foreach (var Letter in LetterArray)
        SymbolList.Add(new Symbol(this, Letter, Weight));
    }

    public int Index { get; }
    public int Score { get; }
    public Symbol[] SymbolDistributionArray { get; internal set; }
    public Inv.DistinctList<Symbol> SymbolList { get; }
  }

  internal sealed class Symbol
  {
    internal Symbol(Tier Tier, char Letter, int Weight)
    {
      this.Tier = Tier;
      this.Letter = Letter;
      this.Weight = Weight;
    }

    public Tier Tier { get; }
    public char Letter { get; }
    public int Weight { get; }
  }
}