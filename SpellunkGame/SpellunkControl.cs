﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  internal sealed class DeckControl : Inv.Panel<Inv.Overlay>
  {
    public DeckControl(Application Application, Inv.Surface Surface)
    {
      this.Application = Application;
      this.Story = Application.Story;
      this.Deck = Application.Deck;

      this.Base = Surface.NewOverlay();
      Base.Border.Set(0, 2, 0, 2).In(Inv.Colour.DimGray);
      Base.AdjustEvent += () =>
      {
        TileCanvas.Draw();
        SelectCanvas.Draw();
      };

      this.AnimationTypeswitch = new Inv.Typeswitch<Inv.DrawContract, Animation>();
      AnimationTypeswitch.Compile(this);

      var BulkheadColour = Inv.Colour.DimGray.Darken(0.50F);

      this.TileCanvas = Surface.NewCanvas();
      Base.AddPanel(TileCanvas);
      TileCanvas.Background.In(BulkheadColour);
      TileCanvas.DrawEvent += (DC) =>
      {
        this.TileDimension = TileCanvas.GetDimension();
        if (TileDimension.Width == 0 || TileDimension.Height == 0)
          return;

        try
        {
          this.TileSize = Math.Min(TileDimension.Width / Deck.Width, TileDimension.Height / Deck.Height);
          this.FontSize = Math.Max(4, TileSize / 2);

          this.TileWidth = TileSize * Deck.Width;
          this.TileHeight = TileSize * Deck.Height;

          this.OffsetX = (TileDimension.Width - TileWidth) / 2;
          this.OffsetY = (TileDimension.Height - TileHeight) / 2;

          var AnimatingTileSet = Story.GetAnimatingTileSet();

          for (var X = 0; X < Deck.Width; X++)
          {
            for (var Y = 0; Y < Deck.Height; Y++)
            {
              var Slot = Deck[X, Y];
              var Tile = Slot.Tile;

              var SlotRect = LocateSlot(Slot);

              if (Tile == null)
              {
                //DC.DrawRectangle(Inv.Colour.Black, null, 0, SlotRect);
              }
              else
              {
                if (AnimatingTileSet.Contains(Tile))
                  continue;

                DrawTile(DC, Tile, SlotRect);
              }
            }
          }

          foreach (var Sequence in Story.GetSequences())
          {
            foreach (var Animation in Sequence.GetAnimations())
            {
              if (Story.Clock < Animation.Start || Story.Clock > Animation.Finish)
                continue;

              AnimationTypeswitch.Execute(DC, Animation);
            }
          }

          // bulkheads so tiles don't look weird when they enter the board.
          // TODO: could be replaced with repeating art per column.
          if (OffsetY > 0)
          {
            var TopBulkhead = new Inv.Rect(0, 0, TileDimension.Width, OffsetY);
            DC.DrawRectangle(BulkheadColour, null, 0, TopBulkhead);
          }

          var BottomHeight = TileDimension.Height - OffsetY - TileHeight;
          if (BottomHeight > 0)
          {
            var BottomBulkhead = new Inv.Rect(0, OffsetY + TileHeight, TileDimension.Width, BottomHeight);
            DC.DrawRectangle(BulkheadColour, null, 0, BottomBulkhead);
          }

#if DEBUG
          //var FrameText = string.Format("{0} x {1} | {2} fps | clock {3}", Base.Surface.Window.Width, Base.Surface.Window.Height, Base.Surface.Window.DisplayRate.PerSecond, Deck.Clock);
          //DC.DrawText(FrameText, "", 10, Inv.FontWeight.Regular, Inv.Colour.WhiteSmoke, new Inv.Point(1, 1), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Top);
#endif
        }
        finally
        {
          Story.AdvanceClock();
        }
      };

      var SelectOpacity = 0.75F;
      var StampColour = Inv.Colour.Orange.Opacity(SelectOpacity);
      var SwapColour = Inv.Colour.Green.Opacity(SelectOpacity);
      var FailColour = Inv.Colour.Red.Opacity(SelectOpacity);
      var VoidColour = Inv.Colour.DodgerBlue.Opacity(SelectOpacity);
      var JumpColour = Inv.Colour.Purple.Opacity(SelectOpacity);

      this.SelectCanvas = Surface.NewCanvas();
      Base.AddPanel(SelectCanvas);
      SelectCanvas.Background.In(Inv.Colour.Transparent);
      SelectCanvas.DrawEvent += (DC) =>
      {
        var EdgeSize = Math.Max(2, TileSize / 8);

        if (SourceSlot != null && TargetSlot != null)
        {
          var SourceRect = LocateSlot(SourceSlot).Reduce(1);
          var TargetRect = LocateSlot(TargetSlot).Reduce(1);

          if (SourceSlot == TargetSlot)
          {
            if (SourceSlot.Tile.Symbol == null)
              DC.DrawRectangle(null, Deck.Stamps > 0 ? StampColour : FailColour, EdgeSize, SourceRect);
            else
              DC.DrawRectangle(null, VoidColour, EdgeSize, SourceRect);
          }
          else if (SourceSlot.Tile != null && TargetSlot.Tile != null)
          {
            if (SourceSlot.IsAdjacent(TargetSlot))
            {
              var Left = Math.Min(SourceRect.Left, TargetRect.Left);
              var Right = Math.Max(SourceRect.Right, TargetRect.Right);
              var Top = Math.Min(SourceRect.Top, TargetRect.Top);
              var Bottom = Math.Max(SourceRect.Bottom, TargetRect.Bottom);

              var AdjacentMove = Deck.Swaps > 0 && !SourceSlot.IsIdentical(TargetSlot) && SourceSlot.Tile.CanMove() && TargetSlot.Tile.CanMove();
              var AdjacentRect = new Inv.Rect(Left, Top, Right - Left + 1, Bottom - Top + 1);
              DC.DrawRectangle(null, AdjacentMove ? SwapColour : FailColour, EdgeSize, AdjacentRect);
            }
            else
            {
              var JumpMove = Deck.Jumps > 0 && !SourceSlot.IsIdentical(TargetSlot) && SourceSlot.IsApart(TargetSlot);

              var DrawColour = JumpMove ? JumpColour : FailColour;

              if (SourceSlot.IsApart(TargetSlot))
              {
                // draw the jump elastic into the middle of each tile.
                var SourcePoint = SourceRect.Center();
                var TargetPoint = TargetRect.Center();
                DC.DrawLine(DrawColour, EdgeSize, Inv.LineJoin.Round, Inv.LineCap.Butt, SourcePoint, TargetPoint);

                // and then re-draw the tile over the ends.
                DrawTile(DC, SourceSlot.Tile, SourceRect);
                DrawTile(DC, TargetSlot.Tile, TargetRect);
              }

              DC.DrawRectangle(null, DrawColour, EdgeSize, SourceRect);
              DC.DrawRectangle(null, DrawColour, EdgeSize, TargetRect);
            }
          }
        }

        if (StampSlot != null)
        {
          var StampRect = LocateSlot(StampSlot).Reduce(1);
          DC.DrawRectangle(null, StampColour, EdgeSize, StampRect);
        }
      };

      var SelectVaried = false;

      SelectCanvas.PressEvent += (Point) =>
      {
        if (Story.IsAnimating())
          return;

        var PressSlot = FindSlot(Deck, Point);

        if (SourceSlot != null && SourceSlot == TargetSlot)
        {
          this.TargetSlot = PressSlot;
          SelectVaried = true;
        }
        else
        {
          this.SourceSlot = null;
          this.TargetSlot = null;

          if (PressSlot != null && PressSlot.Tile != null)
          {
            if (PressSlot.Tile.CanMove())
            {
              this.SourceSlot = PressSlot;
              this.TargetSlot = PressSlot;
              SelectVaried = false;
            }
            else
            {
              Story.Fail(PressSlot);
            }
          }
        }

        SelectCanvas.Draw();
      };
      SelectCanvas.MoveEvent += (Point) =>
      {
        if (Story.IsAnimating())
          return;

        if (SourceSlot != null)
        {
          var MoveSlot = FindSlot(Deck, Point);
          if (MoveSlot != null && MoveSlot.Tile != null && TargetSlot != MoveSlot)
          {
            SelectVaried = true;
            this.TargetSlot = MoveSlot;
          }

          SelectCanvas.Draw();
        }
      };
      SelectCanvas.ReleaseEvent += (Point) =>
      {
        if (Story.IsAnimating())
          return;

        if (SourceSlot != null)
        {
          var ResetSlots = true;

          var ReleaseSlot = FindSlot(Deck, Point);
          if (ReleaseSlot != null && ReleaseSlot.Tile != null)
          {
            this.TargetSlot = ReleaseSlot;

            if (SourceSlot == TargetSlot)
            {
              if (SourceSlot.Tile.Symbol == null)
              {
                if (Deck.Stamps > 0)
                {
                  this.StampSlot = SourceSlot;
                  Application.Stamp();
                }
                else
                {
                  Application.Story.Fail(SourceSlot);
                }
              }
              else
              {
                ResetSlots = SelectVaried;
              }
            }
            else
            {
              if (SourceSlot.IsIdentical(TargetSlot))
              {
                // why swap the exact same tile.
                Application.Story.Fail(SourceSlot, TargetSlot);
              }
              else if (SourceSlot.IsAdjacent(TargetSlot))
              {
                // non-diagonal, immediate neighbour.
                Application.Swap(SourceSlot, TargetSlot);
              }
              else if (SourceSlot.IsApart(TargetSlot))
              {
                // more than one square aware (also can't help with diagonal swaps).
                Application.Jump(SourceSlot, TargetSlot);
              }
              else
              {
                // diagonal swap is the only invalid move.
                Application.Story.Fail(SourceSlot, TargetSlot);
              }
            }
          }

          if (ResetSlots)
          {
            this.SourceSlot = null;
            this.TargetSlot = null;
          }

          SelectCanvas.Draw();
        }
      };
      SelectCanvas.ContextTapEvent += (Point) =>
      {
        var ContextSlot = FindSlot(Deck, Point);

        if (ContextSlot != null && ContextSlot.Tile != null)
          Application.Help().Execute(Application.HelpBook.StylePageDictionary[ContextSlot.Tile.Style]);
      };
    }

    public void Render()
    {
      TileCanvas.Draw();
    }

    internal void Stamp(Symbol Symbol)
    {
      if (StampSlot != null)
      {
        if (Symbol != null)
          Story.Stamp(StampSlot, Symbol);

        this.StampSlot = null;
      }

      SelectCanvas.Draw();
    }

    private Inv.Rect LocateSlot(Slot Slot)
    {
      return new Inv.Rect(Slot.X * TileSize + OffsetX, Slot.Y * TileSize + OffsetY, TileSize, TileSize);
    }
    private Slot FindSlot(Deck Deck, Inv.Point Point)
    {
      var AdjustX = Point.X - OffsetX;
      var AdjustY = Point.Y - OffsetY;

      var TileX = AdjustX / TileSize;
      var TileY = AdjustY / TileSize;

      return Deck.IsValid(TileX, TileY) ? Deck[TileX, TileY] : null;
    }
    private void DrawTile(Inv.DrawContract DC, Tile Tile, Inv.Rect Rect)
    {
      DrawStyle(DC, Tile, Rect);

      if (Tile.Symbol != null)
        DrawLetter(DC, Tile.Symbol.Letter, Rect, Inv.Colour.Black);
    }
    private void DrawStyle(Inv.DrawContract DC, Tile Tile, Inv.Rect Rect, Inv.Colour TintColour = null)
    {
      var Image = Tile.GetImage();

      DC.DrawImage(Image, Rect, 1.0F, TintColour);
    }
    private void DrawLetter(Inv.DrawContract DC, char Letter, Inv.Rect Rect, Inv.Colour Colour)
    {
      //var TileCenter = Rect.Center();
      //DC.DrawText(Letter.ToString(), Application.TileFontName, FontSize, Inv.FontWeight.Regular, Colour.Opacity(0.50F), new Inv.Point(TileCenter.X + 1, TileCenter.Y + 1), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);
      //DC.DrawText(Letter.ToString(), Application.TileFontName, FontSize, Inv.FontWeight.Regular, Colour, TileCenter, Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);

      DC.DrawImage(Theme.GetLetterImage(Letter), Rect, 1.0F, Colour == Inv.Colour.Black ? null : Colour);
    }

#pragma warning disable IDE0051 // Remove unused private members
    private void DrawSwapping(Inv.DrawContract DC, Swapping Animation)
    {
      var ARect = LocateSlot(Animation.A);
      var BRect = LocateSlot(Animation.B);

      var DeltaX = ARect.Left - BRect.Left;
      var DeltaY = ARect.Top - BRect.Top;
      var Progress = Animation.GetProgress();

      var AX = ARect.Left - (int)(+DeltaX * Progress);
      var AY = ARect.Top - (int)(+DeltaY * Progress);

      var BX = BRect.Left - (int)(-DeltaX * Progress);
      var BY = BRect.Top - (int)(-DeltaY * Progress);

      var Expand = (int)(Progress * 4);
      var Double = Expand * 2;

      DrawTile(DC, Animation.B.Tile, new Inv.Rect(AX - Expand, AY - Expand, ARect.Width + Double, ARect.Height + Double));
      DrawTile(DC, Animation.A.Tile, new Inv.Rect(BX - Expand, BY - Expand, BRect.Width + Double, BRect.Height + Double));
    }
    private void DrawFalling(Inv.DrawContract DC, Falling Animation)
    {
      var SlotRect = LocateSlot(Animation.Slot);

      var Progress = Animation.GetProgress();

      var Y = SlotRect.Top - (int)(SlotRect.Height * Progress);

      DrawTile(DC, Animation.Slot.Tile, new Inv.Rect(SlotRect.Left, Y, SlotRect.Width, SlotRect.Height));
    }
    private void DrawStamping(Inv.DrawContract DC, Stamping Animation)
    {
      var SlotRect = LocateSlot(Animation.Slot);
      var SlotTile = Animation.Slot.Tile;

      var Progress = Animation.GetProgress();

      DrawStyle(DC, SlotTile, SlotRect);
      DrawLetter(DC, SlotTile.Symbol.Letter, SlotRect, Inv.Colour.Red.Darken(Progress));
    }
    private void DrawFailing(Inv.DrawContract DC, Failing Animation)
    {
      var SlotRect = LocateSlot(Animation.Slot);

      var Progress = Animation.GetProgress();

      var Expand = (int)(Progress * 4);

      DrawTile(DC, Animation.Slot.Tile, SlotRect.Expand(Expand));
    }
    private void DrawMatching(Inv.DrawContract DC, Matching Animation)
    {
      var Progress = Animation.GetProgress();

      foreach (var Slot in Animation.SlotArray)
      {
        var SlotRect = LocateSlot(Slot);

        DC.DrawImage(Theme.Tiles.Match, SlotRect, 1.0F, Inv.Colour.DarkRed.Opacity(Progress));
        DrawLetter(DC, Slot.Tile.Symbol.Letter, SlotRect, Inv.Colour.White);
      }
    }
    private void DrawScoring(Inv.DrawContract DC, Scoring Animation)
    {
      var Progress = Animation.GetProgress();

      var Slot = Animation.SlotArray[Animation.SlotArray.Length - 1];
      var SlotRect = LocateSlot(Slot);
      var ScorePoint = SlotRect.Center() + new Inv.Point(0, (int)(Progress * (TileSize / 2)));

      DC.DrawText("+" + Animation.Score.ToString(), Inv.DrawFont.New(Application.TileFontName, FontSize, Inv.FontWeight.Bold, Inv.Colour.Black.Opacity(0.50F)), ScorePoint + new Inv.Point(1, 1), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);
      DC.DrawText("+" + Animation.Score.ToString(), Inv.DrawFont.New(Application.TileFontName, FontSize, Inv.FontWeight.Bold, Inv.Colour.White.Opacity(0.50F)), ScorePoint + new Inv.Point(-1, -1), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);
      DC.DrawText("+" + Animation.Score.ToString(), Inv.DrawFont.New(Application.TileFontName, FontSize, Inv.FontWeight.Bold, Inv.Colour.Red), ScorePoint, Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);
    }
#pragma warning disable IDE0060 // Remove unused parameter
    private void DrawEmitting(Inv.DrawContract DC, Emitting Animation)
    {
      if (!Animation.Played)
      {
        Animation.Played = true;

        Application.PlaySound(Animation.Sound);
      }
    }
#pragma warning restore IDE0060 // Remove unused parameter
#pragma warning restore IDE0051 // Remove unused private members

    private readonly Application Application;
    private readonly Inv.Canvas TileCanvas;
    private readonly Inv.Canvas SelectCanvas;
    private readonly Inv.Typeswitch<Inv.DrawContract, Animation> AnimationTypeswitch;
    private readonly Deck Deck;
    private readonly Story Story;
    private int TileSize;
    private int TileWidth;
    private int TileHeight;
    private Inv.Dimension TileDimension;
    private int FontSize;
    private int OffsetX;
    private int OffsetY;
    private Slot StampSlot;
    private Slot SourceSlot;
    private Slot TargetSlot;
  }

  internal sealed class StampControl : Inv.Panel<Inv.Stack>
  {
    public StampControl(Application Application, Inv.Surface Surface)
    {
      this.Base = Surface.NewVerticalStack();
      Base.Alignment.BottomStretch();
      Base.Background.In(Inv.Colour.Black.Opacity(0.50F));
      Base.Padding.Set(5);
      Base.AdjustEvent += () =>
      {
        // TODO: responsive layout.
      };

      var StampTable = Surface.NewHorizontalDock();
      Base.AddPanel(StampTable);

      var SymbolArray = new Symbol[28];
      var SymbolIndex = 0;
      foreach (var Symbol in Application.Bag.GetSymbols())
        SymbolArray[SymbolIndex++] = Symbol;

      foreach (var Symbol in SymbolArray)
      {
        if (StampTable.Clients.Count >= 7)
        {
          StampTable = Surface.NewHorizontalDock();
          Base.AddPanel(StampTable);
        }

        var LetterButton = Surface.NewFlatButton();
        StampTable.AddClient(LetterButton);
        LetterButton.Size.SetHeight(64);
        LetterButton.Margin.Set(4);
        LetterButton.Border.Set(1);
        if (Symbol != null)
        {
          LetterButton.Border.In(Inv.Colour.DarkGoldenrod);
          LetterButton.Background.In(Inv.Colour.BurlyWood);

          var LetterLabel = Surface.NewLabel();
          LetterButton.Content = LetterLabel;
          LetterLabel.Justify.Center();
          LetterLabel.Font.Custom(24).In(Inv.Colour.Black).Name = Application.TileFontName;
          LetterLabel.Text = Symbol.Letter.ToString();

          LetterButton.SingleTapEvent += () =>
          {
            if (SelectEvent != null)
              SelectEvent(Symbol);
          };
        }
      }
    }

    public event Action<Symbol> SelectEvent;
  }

  internal sealed class ScoreControl : Inv.Panel<Inv.Stack>
  {
    internal ScoreControl(Application Application, Inv.Surface Surface)
    {
      this.Base = Surface.NewHorizontalStack();
      Base.Alignment.Center();

      this.DigitArray = new DigitControl[10];

      for (var DigitIndex = 0; DigitIndex < DigitArray.Length; DigitIndex++)
      {
        var Digit = new DigitControl(Application, Surface);
        Base.AddPanel(Digit);

        DigitArray[DigitIndex] = Digit;
      }

      DigitArray[DigitArray.Length - 1].Border.Set(1);
    }

    public void Render(long ScoreValue)
    {
      var ScoreText = ScoreValue.ToString(new string('0', DigitArray.Length));

      var DigitIndex = 0;
      var Zeros = true;

      foreach (var Digit in DigitArray)
      {
        var ScoreDigit = ScoreText[DigitIndex++];

        if (ScoreDigit == '0' && Zeros && DigitIndex < DigitArray.Length)
        {
          Digit.Background.In(Inv.Colour.White.Opacity(0.50F));

          Digit.Text = "";
        }
        else
        {
          Zeros = false;

          Digit.Background.In(Inv.Colour.Silver);

          if ((DigitArray.Length - DigitIndex + 1) % 4 == 0)
            Digit.Font.In(Inv.Colour.Purple);
          else
            Digit.Font.In(Inv.Colour.Black);

          Digit.Text = ScoreDigit.ToString();
        }
      }
    }

    private readonly DigitControl[] DigitArray;
  }

  internal sealed class DigitControl : Inv.Panel<Inv.Label>
  {
    internal DigitControl(Application Application, Inv.Surface Surface)
    {
      this.Base = Surface.NewLabel();
      Base.Size.Set(16, 18);
      Base.Border.Set(1, 1, 0, 1).In(Inv.Colour.LightGray);
      Base.Justify.Center();
      Base.Font.Custom(Theme.NormalSize).Name = Application.TileFontName;
    }

    public Inv.Background Background => Base.Background;
    public Inv.Border Border => Base.Border;
    public Inv.Font Font => Base.Font;
    public string Text
    {
      get => Base.Text;
      set => Base.Text = value;
    }
  }

  internal sealed class HistoryControl : Inv.Panel<Inv.Dock>
  {
    internal HistoryControl(Application Application, Inv.Surface Surface, bool Horizontal)
    {
      this.Application = Application;
      this.Surface = Surface;

      this.WordArray = new WordControl[0];

      this.Base = Horizontal ? Surface.NewHorizontalDock() : Surface.NewVerticalDock();
      Base.Padding.Set(0, 0, 5, 0);
      Base.Background.In(Theme.BackgroundColour);
      Base.AdjustEvent += () =>
      {
        var Dimension = Base.GetDimension();
        if (Dimension == Inv.Dimension.Zero)
          return;

        Adjust(Horizontal, Horizontal ? Dimension.Width : Dimension.Height);
      };

      Adjust(Horizontal, Horizontal ? Surface.Window.Width : Surface.Window.Height);
    }

    public void Reset()
    {
      foreach (var Word in WordArray)
        Word.Reset();

      SetWordCount = 0;
    }
    public void Fade()
    {
      foreach (var Word in WordArray)
        Word.Fade();
    }
    public void Compose()
    {
      Reset();
      AddWords(Application.Deck.GetMoves().SelectMany(Index => Index.WordList).Reverse().Take(WordArray.Length).Reverse());
      Fade();
    }
    public void AddWords(IEnumerable<Word> Words)
    {
      foreach (var Word in Words)
      {
        if (SetWordCount >= WordArray.Length)
        {
          for (var WordIndex = 0; WordIndex < WordArray.Length - 1; WordIndex++)
            WordArray[WordIndex].Copy(WordArray[WordIndex + 1]);

          WordArray[WordArray.Length - 1].Render(Word);
        }
        else
        {
          WordArray[SetWordCount].Render(Word);
        }

        SetWordCount++;
      }
    }

    private void Adjust(bool Horizontal, int Size)
    {
      var OldWordCount = WordArray != null ? WordArray.Length : 0;
      var NewWordCount = Horizontal ? Size / 100 : Size / 40;

      if (NewWordCount != OldWordCount)
      {
        for (var WordIndex = NewWordCount; WordIndex < OldWordCount; WordIndex++)
          Base.RemoveClient(Base.Clients.Last());

        Array.Resize(ref WordArray, NewWordCount);

        for (var WordIndex = OldWordCount; WordIndex < NewWordCount; WordIndex++)
        {
          var Word = new WordControl(Application, Surface);
          Base.AddClient(Word);

          WordArray[WordIndex] = Word;
        }

        Compose();
      }
    }

    private readonly Application Application;
    private readonly Inv.Surface Surface;
    private WordControl[] WordArray;
    private int SetWordCount;
  }

  internal sealed class WordControl : Inv.Panel<Inv.Button>
  {
    internal WordControl(Application Application, Inv.Surface Surface)
    {
      this.Base = Surface.NewFlatButton();
      Base.Margin.Set(5, 5, 0, 0);
      Base.Padding.Set(5);
      Base.Corner.Set(1);
      Base.Border.Set(1).In(Inv.Colour.DimGray);
      Base.IsFocusable = false;
      Base.IsEnabled = false;
      Base.SingleTapEvent += () => Application.Dictionary().Execute(WordLabel.Text.ToLower());

      var LayoutDock = Surface.NewHorizontalDock();
      Base.Content = LayoutDock;

      this.WordLabel = Surface.NewLabel();
      LayoutDock.AddClient(WordLabel);

      this.ScoreLabel = Surface.NewLabel();
      LayoutDock.AddFooter(ScoreLabel);
      ScoreLabel.Font.In(Inv.Colour.DarkRed);
    }

    public void Copy(WordControl Control)
    {
      WordLabel.Text = Control.WordLabel.Text;
      ScoreLabel.Text = Control.ScoreLabel.Text;
      Base.Background.In(Control.Base.Background.Colour);
      Base.IsEnabled = Control.Base.IsEnabled;
    }
    public void Render(Word Word)
    {
      WordLabel.Text = Word.Text;
      ScoreLabel.Text = Word.Score.ToString();
      Base.Background.In(Inv.Colour.WhiteSmoke);
      Base.IsEnabled = true;
    }
    public void Fade()
    {
      Base.Background.In(Theme.BackgroundColour);
    }
    public void Reset()
    {
      Fade();
      WordLabel.Text = null;
      ScoreLabel.Text = null;
      Base.IsEnabled = false;
    }

    private readonly Inv.Label WordLabel;
    private readonly Inv.Label ScoreLabel;
  }

  internal sealed class DashControl : Inv.Panel<Inv.Button>
  {
    internal DashControl(Application Application, string Title, Inv.Surface Surface, HelpPage HelpPage)
    {
      this.Base = Surface.NewFlatButton();
      Base.Alignment.Center();
      Base.IsFocusable = false;
      Base.Border.Set(1).In(Inv.Colour.DarkGray);
      Base.Corner.Set(1);
      Base.Size.Set(60);
      Base.Background.In(Inv.Colour.WhiteSmoke);
      Base.Margin.Set(5, 5, 0, 0);
      Base.SingleTapEvent += () => Application.Help().Execute(HelpPage);
      
      var LayoutDock = Surface.NewVerticalDock();
      Base.Content = LayoutDock;

      this.CountLabel = Surface.NewLabel();
      LayoutDock.AddClient(CountLabel);
      CountLabel.Alignment.Center();
      CountLabel.Font.Custom(20);
      CountLabel.LineWrapping = false;
      CountLabel.Text = "0";

      this.TitleLabel = Surface.NewLabel();
      LayoutDock.AddFooter(TitleLabel);
      TitleLabel.Alignment.Center();
      TitleLabel.Font.SmallCaps().In(Inv.Colour.DarkRed);
      TitleLabel.LineWrapping = false;
      TitleLabel.Text = Title;
    }

    public long Count
    {
      get => int.Parse(CountLabel.Text);
      set
      {
        CountLabel.Text = value.ToString();

        if (value <= 0)
        {
          Base.Opacity.Set(0.25F);
        }
        else
        {
          Base.Opacity.Set(1.00F);
        }

        if (value < CountField)
          Base.Background.In(Inv.Colour.LightCoral);
        else if (value > CountField)
          Base.Background.In(Inv.Colour.LightGreen);
        else
          Base.Background.In(Inv.Colour.WhiteSmoke);

        CountField = value;
      }
    }

    public void Reset()
    {
      CountField = null;
    }

    private readonly Inv.Label TitleLabel;
    private readonly Inv.Label CountLabel;
    private long? CountField;
  }

  internal sealed class TileControl : Inv.Panel<Inv.Overlay>
  {
    internal TileControl(Application Application, Inv.Surface Surface, Tile Tile)
    {
      this.Base = Surface.NewOverlay();
      Base.Margin.Set(4);
      Base.Size.Set(92);

      var Graphic = Surface.NewGraphic();
      Base.AddPanel(Graphic);
      Graphic.Image = Tile.GetImage();

      var Label = Surface.NewLabel();
      Base.AddPanel(Label);
      Label.Elevation.Set(1);
      Label.Justify.Center();
      Label.Font.Custom(48).In(Inv.Colour.Black).Name = Application.TileFontName;
      Label.Text = Tile.Symbol?.Letter.ToString();
    }
  }
}
