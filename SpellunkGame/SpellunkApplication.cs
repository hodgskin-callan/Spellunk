﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  internal sealed class Application : Inv.Mimic<Inv.Application>
  {
    internal Application(Inv.Application Base)
    {
      this.Base = Base;
      Base.Title = "Spellunk";

      var RootFolder = Base.Directory.Root;

      this.SaveFile = RootFolder.NewFile("Spellunk.save");
      this.WorkingFile = RootFolder.NewFile("Spellunk.working");
      this.RecordFile = RootFolder.NewFile("Spellunk.record");
      this.LadderFile = RootFolder.NewFile("Spellunk.ladder");
      this.SettingsFile = RootFolder.NewFile("Spellunk.settings");
      this.LogsFolder = RootFolder.NewFolder("Logs");
      this.FaultFile = LogsFolder.NewFile("Spellunk." + Version + ".fault.log");

      Base.HandleExceptionEvent += (Exception) => HandleException(Exception);

#if DEBUG
      //HandleException(new Exception("Testing message"));
#endif

      this.Bag = new Bag();
      this.Deck = new Deck(Bag);
      this.Story = new Story(Deck);
      this.DictionarySet = new HashSet<string>();

      var DictionaryAsset = Base.Directory.NewAsset("Spellunk.dictionary");

      if (DictionaryAsset.Exists())
      {
        using (var DictionaryStream = DictionaryAsset.Open())
        using (var DictionaryReader = new System.IO.StreamReader(DictionaryStream))
        {
          while (!DictionaryReader.EndOfStream)
            DictionarySet.Add(DictionaryReader.ReadLine());
        }
      }

      this.Settings = new Settings();
      LoadSettings();

      this.HelpBook = new HelpBook(this);

      var Binding = Base.Keyboard.NewBinding();
      Binding.KeyPressEvent += (Keystroke) =>
      {
        if (Overlay.Panels.Contains(StampControl))
        {
          if (Keystroke.Key == Inv.Key.Escape)
            StampShade.SingleTap();
          else if (Keystroke.Key >= Inv.Key.A && Keystroke.Key <= Inv.Key.Z)
            SelectStamp(Bag.SymbolByLetter((char)('A' + ((int)Keystroke.Key - (int)Inv.Key.A))));
        }
      };

      this.Surface = Base.Window.NewSurface();
      Surface.EnterEvent += () => Binding.Capture();
      Surface.LeaveEvent += () => Binding.Release();

      this.Overlay = Surface.NewOverlay();
      Surface.Content = Overlay;
      Overlay.Background.In(Theme.BackgroundColour);

      var MainDock = Surface.NewVerticalDock();
      Overlay.AddPanel(MainDock);

      this.HeaderOverlay = Surface.NewOverlay();
      HeaderOverlay.Background.In(Theme.BackgroundColour);
      HeaderOverlay.Border.Set(0, 1, 0, 0).In(Theme.BorderColour);

      //var HeaderGraphic = Surface.NewGraphic();
      //HeaderOverlay.AddPanel(HeaderGraphic);
      //HeaderGraphic.Image = Resources.Images.Strip;

      var HeaderDock = Surface.NewHorizontalDock();
      HeaderOverlay.AddPanel(HeaderDock);

      var PortraitHistoryControl = new HistoryControl(this, Surface, true);
      var LandscapeHistoryControl = new HistoryControl(this, Surface, false);

      this.HistoryControl = PortraitHistoryControl;

      var ProductLabel = Surface.NewLabel();
      ProductLabel.Padding.Set(5, 0, 5, 0);
      ProductLabel.Justify.Center();
      ProductLabel.Font.Custom(Theme.MassiveSize).SmallCaps();
      ProductLabel.Text = "Spellunk";

      this.ScoreControl = new ScoreControl(this, Surface);

      this.LevelLabel = Surface.NewLabel();
      LevelLabel.Justify.Center();
      LevelLabel.Font.Custom(Theme.MassiveSize).SmallCaps();

      this.WordsLabel = Surface.NewLabel();
      WordsLabel.Justify.Center();
      WordsLabel.Font.Custom(Theme.ExtraLargeSize).In(Inv.Colour.DarkRed);

#if DEBUG
      this.DebugLabel = Surface.NewLabel();
      DebugLabel.Justify.Center();
      DebugLabel.Font.Custom(Theme.LargeSize).In(Inv.Colour.Black);
#endif

      this.FooterOverlay = Surface.NewOverlay();
      FooterOverlay.Border.Set(0, 1, 0, 0).In(Theme.BorderColour);
      FooterOverlay.Background.In(Theme.BackgroundColour);

      var FooterDock = Surface.NewHorizontalDock();
      FooterOverlay.AddPanel(FooterDock);

      this.MenuButton = Surface.NewFlatButton();
      FooterDock.AddHeader(MenuButton);
      MenuButton.Margin.Set(5, 5, 5, 0);
      MenuButton.Background.In(Theme.BackgroundColour);
      MenuButton.Alignment.Center();
      MenuButton.Hint = "Menu";
      MenuButton.SingleTapEvent += () => Menu();

      var MenuGraphic = Surface.NewGraphic();
      MenuButton.Content = MenuGraphic;
      MenuGraphic.Image = Resources.Icons.Menu;
      MenuGraphic.Size.Set(48);

      this.HelpButton = Surface.NewFlatButton();
      FooterDock.AddFooter(HelpButton);
      HelpButton.Margin.Set(5, 5, 5, 0);
      HelpButton.Background.In(Theme.BackgroundColour);
      HelpButton.Alignment.Center();
      HelpButton.Hint = "Menu";
      HelpButton.SingleTapEvent += () => Help().Execute();

      var HelpGraphic = Surface.NewGraphic();
      HelpButton.Content = HelpGraphic;
      HelpGraphic.Image = Resources.Icons.Help;
      HelpGraphic.Size.Set(48);

      var MoveStack = Surface.NewHorizontalStack();
      FooterDock.AddClient(MoveStack);
      MoveStack.Alignment.Center();
      MoveStack.Margin.Set(0, 0, 5, 5);

      this.SwapMoveDash = new DashControl(this, "Swaps", Surface, HelpBook.SwapMovePage);
      MoveStack.AddPanel(SwapMoveDash);

      this.StampMoveDash = new DashControl(this, "Stamps", Surface, HelpBook.StampMovePage);
      MoveStack.AddPanel(StampMoveDash);

      this.JumpMoveDash = new DashControl(this, "Jumps", Surface, HelpBook.JumpMovePage);
      MoveStack.AddPanel(JumpMoveDash);

      this.DeckControl = new DeckControl(this, Surface);

      var LoadedGame = SaveFile.Exists();

      if (LoadedGame)
      {
        try
        {
          Syntax.LoadDeck(Deck, SaveFile);
        }
        catch (Exception Exception)
        {
          HandleException(Exception);

          LoadedGame = false;
        }
      }

      if (LoadedGame)
      {
        RenderGame();

        HistoryControl.Compose();
      }
      else
      {
        NewGame();
      }

      Surface.ArrangeEvent += () =>
      {
        if (Surface.Window.Width < Surface.Window.Height && (MainDock.PanelCount == 0 || !MainDock.IsVertical))
        {
          this.HistoryControl = PortraitHistoryControl;

          MainDock.SetVertical();
          MainDock.RemovePanels();
          HeaderDock.RemovePanels();

          HeaderDock.Padding.Clear();
          HeaderDock.SetHorizontal();
          HeaderDock.AddHeader(ProductLabel);
          HeaderDock.AddFooter(LevelLabel);
          HeaderDock.AddFooter(WordsLabel);
#if DEBUG
          HeaderDock.AddFooter(DebugLabel);
#endif

          MainDock.AddHeader(HeaderOverlay);
          MainDock.AddHeader(ScoreControl);
          MainDock.AddHeader(HistoryControl);
          MainDock.AddClient(DeckControl);
          MainDock.AddFooter(FooterOverlay);

          FooterDock.Margin.Clear();
          FooterDock.SetHorizontal();
          MoveStack.SetHorizontal();
        }
        else if (Surface.Window.Width >= Surface.Window.Height && (MainDock.PanelCount == 0 || !MainDock.IsHorizontal))
        {
          this.HistoryControl = LandscapeHistoryControl;

          MainDock.SetHorizontal();
          MainDock.RemovePanels();
          HeaderDock.RemovePanels();

          HeaderDock.Padding.Set(0, 0, 0, 5);
          HeaderDock.SetVertical();
          HeaderDock.AddHeader(ProductLabel);
          HeaderDock.AddHeader(ScoreControl);
          HeaderDock.AddClient(HistoryControl);
          HeaderDock.AddFooter(LevelLabel);
          HeaderDock.AddFooter(WordsLabel);
#if DEBUG
          HeaderDock.AddFooter(DebugLabel);
#endif

          MainDock.AddHeader(HeaderOverlay);
          MainDock.AddClient(DeckControl);
          MainDock.AddFooter(FooterOverlay);

          FooterDock.Margin.Set(0, 5, 0, 5);
          MoveStack.SetVertical();
          FooterDock.SetVertical();
        }

        // images are not ready the first time we render.
        if (Base.Device.IsBlazorWebAssembly)
          Base.Window.Post(() => RenderGame());
      };

      var WasAnimating = false;

      Surface.ComposeEvent += () =>
      {
#if DEBUG
        this.DebugLabel.Text = Base.Window.DisplayRate.PerSecond.ToString();
#endif

        // draw one last time after the animations are all finished.
        var IsAnimating = Story.IsAnimating();

        if (WasAnimating && !IsAnimating)
        {
          RenderGame();
          Complete();
        }
        else if (WasAnimating || IsAnimating)
        {
          DeckControl.Render();
        }

        WasAnimating = IsAnimating;
      };

      Base.SuspendEvent += () => SaveGame();
      Base.StartEvent += () => Base.Window.Transition(Surface);
      Base.StopEvent += () => SaveGame();
    }

    internal Settings Settings { get; }
    internal Bag Bag { get; }
    internal Deck Deck { get; }
    internal Story Story { get; }
    internal HelpBook HelpBook { get; }
    internal Inv.Surface Surface { get; }
    internal string TileFontName => Base.Device.MonospacedFontName;
    internal string Version => Resources.Texts.Version;

    internal readonly string AuthorName = "Callan Hodgskin";
    internal readonly string AuthorEmail = "hodgskin.callan@gmail.com";

    internal Screen NewScreen()
    {
      return new Screen(this, Base.Window.NewSurface());
    }
    internal void NewGame()
    {
      ResetGame();

      Deck.Clear();
      Deck.StartTime = DateTimeOffset.Now;
      Deck.Words = 10;
      Deck.Swaps = 20;
      Deck.Stamps = 5;
      Deck.Jumps = 0;
      Deck.Score = 0;
      Deck.Level = 1;
      Deck.ClearMoves();

      Generate();

      SaveGame();

      RenderGame();
    }
    internal void EndGame()
    {
      PlaySound(Resources.Sounds.EndGame);

      var Record = new Record()
      {
        BeginTime = Deck.StartTime,
        EndTime = DateTimeOffset.Now,
        Name = Base.Device.Name,
        Level = Deck.Level,
        Score = Deck.Score,
        Words = Deck.GetMoves().Sum(Index => Index.WordList.Count),
        Moves = Deck.GetMoves().Count()
      };

      try
      {
        Syntax.SaveRecord(Record, RecordFile);
      }
      catch (Exception Exception)
      {
        HandleException(Exception);
      }

      ViewLadder(Record);
    }
    internal void BeginGame()
    {
      Generate();

      SaveGame();

      RenderGame();
    }
    internal void RenderGame()
    {
      RenderScores();
      RenderMoves();
      DeckControl.Render();
    }
    internal void ViewLadder(Record Record)
    {
      var Ladder = new Ladder();

      if (LadderFile.Exists())
      {
        try
        {
          Syntax.LoadLadder(Ladder, LadderFile);
        }
        catch (Exception Exception)
        {
          HandleException(Exception);

          Ladder.RecordList.Clear();
        }
      }

      if (Record != null && Record.Score > 0 && !Ladder.RecordList.Exists(Index => Index.BeginTime == Record.BeginTime && Index.Score == Record.Score))
      {
        var RecordIndex = 0;
        while (RecordIndex < Ladder.RecordList.Count && Ladder.RecordList[RecordIndex].Score > Record.Score)
          RecordIndex++;

        const int LadderRecordLimit = 10;

        if (RecordIndex < LadderRecordLimit)
        {
          Ladder.RecordList.Insert(RecordIndex, Record);
          while (Ladder.RecordList.Count > LadderRecordLimit)
            Ladder.RecordList.RemoveAt(Ladder.RecordList.Count - 1);
        }
      }

      if (LadderScreen == null)
        this.LadderScreen = new LadderScreen(this);
      LadderScreen.Execute(Record, Ladder);
    }
    internal void Stamp()
    {
      PlaySound(Resources.Sounds.Click);

      if (StampShade == null)
      {
        this.StampShade = Surface.NewStarkButton();
        StampShade.Background.In(Inv.Colour.Transparent);
        StampShade.SingleTapEvent += () =>
        {
          PlaySound(Resources.Sounds.Click);

          Overlay.RemovePanel(StampShade);
          Overlay.RemovePanel(StampControl);

          DeckControl.Stamp(null);
        };
      }

      if (StampControl == null)
      {
        this.StampControl = new StampControl(this, Surface);
        StampControl.SelectEvent += (Symbol) => SelectStamp(Symbol);
      }

      if (!Overlay.Panels.Contains(StampShade))
      {
        Overlay.AddPanel(StampShade);
        Overlay.AddPanel(StampControl);
      }
    }
    internal void Swap(Slot A, Slot B)
    {
      Story.Swap(A, B);

      RenderGame();
    }
    internal void Jump(Slot A, Slot B)
    {
      Story.Jump(A, B);

      RenderGame();
    }
    internal void PlaySound(Inv.Sound Sound)
    {
      Base.Audio.Play(Sound);
    }
    internal HelpScreen Help()
    {
      if (HelpScreen == null)
        this.HelpScreen = new HelpScreen(this);
      return HelpScreen;
    }
    internal DictionaryScreen Dictionary()
    {
      if (DictionaryScreen == null)
        this.DictionaryScreen = new DictionaryScreen(this);
      return DictionaryScreen;
    }
    internal void HandleException(Exception Exception)
    {
      using (var FaultStream = FaultFile.AsText().Append())
        FaultStream.WriteLine(Exception.AsReport());
    }
    internal void SendFaults()
    {
      if (FaultFile.Exists())
      {
        // append new faults to the sent log file.
        var SentLogFile = LogsFolder.NewFile(FaultFile.Name + ".sent.log");

        using (var InputStream = FaultFile.Open())
        using (var OutputStream = SentLogFile.Append())
          InputStream.CopyTo(OutputStream);

        // if we sent the email we can delete the current fault log file.

        var Message = Base.Email.NewMessage();
        Message.Subject = "Spellunk Faults: " + Version;
        Message.Body = "<Please offer any clarification>" + FeedbackSignature();
        Message.To(AuthorName, AuthorEmail);
        Message.Attach(FaultFile.Name, FaultFile);

        if (Message.Send())
          FaultFile.Delete();
      }
    }
    internal void SendFeedback()
    {
      var EmailMessage = Surface.Window.Application.Email.NewMessage();
      EmailMessage.To(AuthorName, AuthorEmail);
      EmailMessage.Subject = "Spellunk Feedback";
      EmailMessage.Body = "<Please provide your feedback>" + FeedbackSignature();
      EmailMessage.Send();
    }
    internal bool HasFaults()
    {
      return FaultFile.Exists();
    }
    internal void SaveLadder(Ladder Ladder)
    {
      try
      {
        Syntax.SaveLadder(Ladder, LadderFile);
      }
      catch (Exception Exception)
      {
        HandleException(Exception);
      }
    }
    internal void SaveSettings()
    {
      try
      {
        if (SettingsFile.Exists())
        {
          using (var IniWriter = SettingsFile.AsIni().Create())
          {
            IniWriter.WriteTuple("mute", Settings.Mute.ToString());
          }
        }
      }
      catch (Exception Exception)
      {
        HandleException(Exception);
      }

      ApplySettings();
    }

    private void LoadSettings()
    {
      try
      {
        if (SettingsFile.Exists())
        {
          using (var IniReader = SettingsFile.AsIni().Open())
          {
            Settings.Mute = bool.Parse(IniReader.ReadTuple("mute"));
          }
        }
      }
      catch (Exception Exception)
      {
        HandleException(Exception);
      }

      ApplySettings();
    }
    private void ApplySettings()
    {
      Base.Audio.SetMuted(Settings.Mute);
    }
    private void ResetGame()
    {
      HistoryControl.Reset();
      SwapMoveDash.Reset();
      StampMoveDash.Reset();
      JumpMoveDash.Reset();
    }
    private void RenderScores()
    {
      LevelLabel.Text = "Level " + Deck.Level;
      WordsLabel.Text = "[ " + (Deck.Words < 0 ? "!" : Deck.Words.ToString()) + " ]";

      ScoreControl.Render(Deck.Score);
    }
    private void RenderMoves()
    {
      if (SwapMoveDash.Count != Deck.Swaps || StampMoveDash.Count != Deck.Stamps || JumpMoveDash.Count != Deck.Jumps)
      {
        SwapMoveDash.Count = Deck.Swaps;
        StampMoveDash.Count = Deck.Stamps;
        JumpMoveDash.Count = Deck.Jumps;
      }
    }
    private void Menu()
    {
      if (MenuScreen == null)
        this.MenuScreen = new MenuScreen(this);

      MenuScreen.Execute();
    }
    private void Generate()
    {

      // Replace all tiles.
      foreach (var Slot in Deck.GetSlots())
        Slot.Tile = Bag.GenerateTile();

      // Cascade until we have a stable board.
      var WordList = new Inv.DistinctList<Word>();
      while (CheckGrid(WordList))
      {
        foreach (var Word in WordList)
        {
          foreach (var Slot in Word.SlotList)
            Slot.Tile = Bag.GenerateTile();
        }
      }
    }
    private void Complete()
    {
      var Time = DateTimeOffset.Now;

      var WordList = new Inv.DistinctList<Word>();
      if (CheckGrid(WordList))
      {
        HistoryControl.Fade();

        Story.Match(WordList);

        HistoryControl.AddWords(WordList);
      }

      if (!Story.IsAnimating())
      {
        if (Deck.Swaps <= 0 && Deck.Stamps <= 0 && Deck.Jumps <= 0)
        {
          EndGame();
        }
        else if (Deck.Words <= 0)
        {
          Story.NextLevel(() =>
          {
            // TODO: next level animation?
            Deck.Level++;
            Deck.Words = 5 + (Deck.Level * 5);

            // Ensure a minimum starting swaps per level.
            if (Deck.Swaps <= Deck.Level)
              Deck.Swaps = Deck.Level;

            if (AdvanceScreen == null)
              this.AdvanceScreen = new AdvanceScreen(this);
            AdvanceScreen.Execute();
          });
        }
      }
    }
    private bool CheckGrid(Inv.DistinctList<Word> WordList)
    {
      WordList.Clear();

      var RowSlotArray = new Slot[Deck.Width];

      for (var Row = 0; Row < Deck.Height; Row++)
      {
        for (var Column = 0; Column < Deck.Width; Column++)
          RowSlotArray[Column] = Deck[Row, Column];

        CheckArray(RowSlotArray, WordList);
      }

      var ColumnSlotArray = new Slot[Deck.Height];

      for (var Column = 0; Column < Deck.Width; Column++)
      {
        for (var Row = 0; Row < Deck.Height; Row++)
          ColumnSlotArray[Row] = Deck[Row, Column];

        CheckArray(ColumnSlotArray, WordList);
      }

      return WordList.Count > 0;
    }
    private void CheckArray(Slot[] SlotArray, Inv.DistinctList<Word> WordList)
    {
      var SlotUsedSet = new HashSet<Slot>();

      for (var WordLength = SlotArray.Length; WordLength >= Rules.MinimumWordLength; WordLength--)
      {
        var WordIndex = 0;

        while (WordIndex + WordLength <= SlotArray.Length)
        {
          var CheckList = SlotArray.Skip(WordIndex).Take(WordLength).ToDistinctList();

          if (CheckList.All(Index => Index.Tile != null && Index.Tile.Symbol != null) && CheckList.Any(Index => !SlotUsedSet.Contains(Index)))
          {
            var Word = new string(CheckList.Select(Index => Index.Tile.Symbol.Letter).ToArray());

            if (CheckSpelling(Word))
            {
              WordList.Add(new Word()
              {
                Text = Word,
                Score = Calculate(CheckList),
                SlotList = CheckList
              });

              SlotUsedSet.AddRange(CheckList);
            }
          }

          WordIndex++;
        }
      }
    }
    private bool CheckSpelling(string Word)
    {
      Debug.Assert(Word.IsUpperOrNonLetter());

      return DictionarySet.Contains(Word);
    }
    private int Calculate(Inv.DistinctList<Slot> SquareList)
    {
      var Multiplier = 1;
      var Bonus = 0;
      var Score = 0;

      foreach (var Square in SquareList)
      {
        if (Square.Tile.Style != Style.Wooden)
        {
          Score += Square.Tile.Symbol.Tier.Score;

          switch (Square.Tile.Style)
          {
            case Style.Broken:
              Bonus += 20;
              break;

            case Style.Molten:
              Bonus += 10;
              break;

            case Style.Silver:
              Multiplier *= 1;
              break;

            case Style.Gold:
              Multiplier *= 2;
              break;

            case Style.Platinum:
              Multiplier *= 3;
              break;

            default:
              throw new Exception("Style not handled.");
          }
        }
      }

      return (Score * SquareList.Count * Multiplier) + Bonus;
    }
    private void SaveGame()
    {
      Retry("SaveGame", 5, TimeSpan.FromMilliseconds(200), () =>
      {
        // attempt to avoid 'sharing violation' on Android.
        if (WorkingFile.Exists())
          WorkingFile.Delete();

        Syntax.SaveDeck(Deck, WorkingFile);

        if (SaveFile.Exists())
          WorkingFile.Replace(SaveFile);
        else
          WorkingFile.Move(SaveFile);
      });
    }
    private void SelectStamp(Symbol Symbol)
    {
      DeckControl.Stamp(Symbol);

      RenderGame();

      StampShade.SingleTap();
    }
    private string FeedbackSignature()
    {
      var Device = Base.Device;
      var Window = Base.Window;

      return
        Environment.NewLine +
        Environment.NewLine +
        Environment.NewLine +
        "version: " + Version + Environment.NewLine +
        "culture: " + System.Globalization.CultureInfo.CurrentCulture.Name + Environment.NewLine +
        "device: " + Device.Model + " [" + Device.System + "] " + Window.Width + "x" + Window.Height;
      ;
    }
    private void Retry(string Operation, int Times, TimeSpan Grace, Action Action)
    {
      var Loop = 0;

      List<Exception> ExceptionList = null;

      while (Loop < Times)
      {
        Loop++;

        try
        {
          Action();

          break;
        }
        catch (Exception Exception)
        {
          if (ExceptionList == null)
            ExceptionList = new List<Exception>();
          ExceptionList.Add(Exception);

          if (Loop >= Times)
            HandleException(new AggregateException(Operation + " failed " + Times + " times and was aborted.", ExceptionList));
          else
            Base.Window.Sleep(Grace);
        }
      }
    }

    private readonly Inv.File SaveFile;
    private readonly Inv.File WorkingFile;
    private readonly Inv.File RecordFile;
    private readonly Inv.File LadderFile;
    private readonly Inv.File SettingsFile;
    private readonly Inv.File FaultFile;
    private readonly Inv.Folder LogsFolder;
    private readonly Inv.Overlay Overlay;
    private readonly DeckControl DeckControl;
    private readonly Inv.Label LevelLabel;
    private readonly Inv.Label WordsLabel;
#if DEBUG
    private readonly Inv.Label DebugLabel;
#endif
    private readonly DashControl SwapMoveDash;
    private readonly DashControl StampMoveDash;
    private readonly DashControl JumpMoveDash;
    private readonly Inv.Button MenuButton;
    private readonly Inv.Button HelpButton;
    private readonly HashSet<string> DictionarySet;
    private readonly Inv.Overlay HeaderOverlay;
    private readonly Inv.Overlay FooterOverlay;
    private readonly ScoreControl ScoreControl;
    private MenuScreen MenuScreen;
    private HelpScreen HelpScreen;
    private AdvanceScreen AdvanceScreen;
    private LadderScreen LadderScreen;
    private DictionaryScreen DictionaryScreen;
    private StampControl StampControl;
    private Inv.Button StampShade;
    private HistoryControl HistoryControl;
  }

  internal sealed class Screen
  {
    internal Screen(Application Application, Inv.Surface Surface)
    {
      this.Application = Application;
      this.Surface = Surface;

      var LayoutDock = Surface.NewVerticalDock();
      Surface.Content = LayoutDock;

      this.TitleLabel = Surface.NewLabel();
      LayoutDock.AddHeader(TitleLabel);
      TitleLabel.Background.In(Inv.Colour.DimGray);
      TitleLabel.Border.Set(0, 0, 0, 1).In(Theme.BorderColour);
      TitleLabel.Padding.Set(5);
      TitleLabel.Justify.Center();
      TitleLabel.Font.Custom(Theme.MassiveSize).SmallCaps().In(Inv.Colour.White);

      this.LayoutFrame = Surface.NewFrame();
      LayoutDock.AddClient(LayoutFrame);
      LayoutFrame.Background.In(Theme.BackgroundColour);

      var FooterFrame = Surface.NewFrame();
      LayoutDock.AddFooter(FooterFrame);

      this.CloseButton = Surface.NewFlatButton();
      FooterFrame.Content = CloseButton;
      CloseButton.Alignment.Center();
      CloseButton.Background.In(Inv.Colour.DarkGray);
      CloseButton.Padding.Set(10);
      CloseButton.Visibility.Collapse();
      CloseButton.SingleTapEvent += () => Close();

      this.CloseLabel = Surface.NewLabel();
      CloseButton.Content = CloseLabel;
      CloseLabel.Justify.Center();
      CloseLabel.Font.Custom(Theme.ExtraLargeSize);
    }

    public Inv.Surface Surface { get; }
    public string Title
    {
      get => TitleLabel.Text;
      set => TitleLabel.Text = value;
    }
    public string Caption
    {
      get => CloseLabel.Text;
      set
      {
        CloseLabel.Text = value;
        CloseButton.Visibility.Set(value != null);
      }
    }
    public event Action CloseEvent
    {
      add { CloseButton.SingleTapEvent += value; }
      remove { CloseButton.SingleTapEvent -= value; }
    }
    public Inv.Panel Content
    {
      get => LayoutFrame.Content;
      set => LayoutFrame.Content = value;
    }

    internal Application Application { get; }

    internal void Open()
    {
      Surface.Window.Transition(Surface).Fade();
    }
    internal void Close()
    {
      Surface.Window.Transition(Application.Surface).Fade();
    }

    private readonly Inv.Label TitleLabel;
    private readonly Inv.Frame LayoutFrame;
    private readonly Inv.Button CloseButton;
    private readonly Inv.Label CloseLabel;
  }

  internal static class Theme
  {
    static Theme()
    {
      LetterArray = new Inv.Image[26];

      var LetterImages = Resources.SegoeUI;
      var LetterType = LetterImages.GetType().GetReflectionInfo();

      var LetterIndex = 0;
      for (var Letter = 'A'; Letter <= 'Z'; Letter++)
        LetterArray[LetterIndex++] = (Inv.Resource.ImageReference)LetterType.GetReflectionField(Letter.ToString()).GetValue(LetterImages);
    }

    public static readonly ResourcesTiles128 Tiles = Resources.Tiles128;
    public static Inv.Image GetLetterImage(char Letter)
    {
      return LetterArray[(int)Letter - (int)'A'];
    }

    public static readonly Inv.Colour BorderColour = Inv.Colour.DarkGray;
    public static readonly Inv.Colour BackgroundColour = Inv.Colour.LightGray;

    public const int NormalSize = 12;
    public const int LargeSize = 16;
    public const int ExtraLargeSize = 20;
    public const int MassiveSize = 24;

    private static readonly Inv.Image[] LetterArray;
  }

  internal sealed class Settings
  {
    public bool Mute { get; set; }
  }
}