﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv.Manual
{
  public class Program
  {
    [STAThread]
    static void Main()
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.Options.DeviceEmulation = Inv.WpfDeviceEmulation.iPhone5;
        Inv.WpfShell.Options.PreventDeviceEmulation = false;
        Inv.WpfShell.Options.DeviceEmulationRotated = false;
        Inv.WpfShell.Options.DeviceEmulationFramed = true;
        Inv.WpfShell.Options.FullScreenMode = false;
        Inv.WpfShell.Options.DefaultWindowWidth = 1920;
        Inv.WpfShell.Options.DefaultWindowHeight = 1080;

        Inv.WpfShell.Run(Spellunk.Shell.Install);
      });
    }
  }
}