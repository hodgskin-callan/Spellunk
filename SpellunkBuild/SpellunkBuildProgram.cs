﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Spellunk
{
  class BuildProgram
  {
    [STAThread]
    static void Main()
    {
      Inv.BuildShell.Run(new PublishScript());
    }
  }

  public sealed class PublishScript : Inv.Mimic<Inv.BuildScript>
  {
    public PublishScript()
    {
      this.Base = new Inv.BuildScript("Spellunk Publish");

      const string MountPath = @"C:\Development\Spellunk\";
      const string DeployPath = @"C:\Deployment\Spellunk\";

      var SpellunkSolution = Base.SelectSolution(MountPath + "Spellunk.sln");
      var SpellunkGameProject = Base.SelectProject(MountPath + @"SpellunkGame\SpellunkGame.csproj");
      var SpellunkGameWTarget = SpellunkSolution.SelectTarget("SpellunkGameW");

      var SpellunkGameIProject = Base.SelectProject(MountPath + @"SpellunkGameI\SpellunkGameI.csproj");
      var SpellunkGameITarget = SpellunkSolution.SelectTarget("SpellunkGameI");
      var SpellunkGameIManifest = SpellunkGameIProject.SelectiOSManifest();

      var SpellunkGameAProject = Base.SelectProject(MountPath + @"SpellunkGameA\SpellunkGameA.csproj");
      var SpellunkGameATarget = SpellunkSolution.SelectTarget("SpellunkGameA");
      var SpellunkGameAManifest = SpellunkGameAProject.SelectAndroidManifest();

      var SpellunkGameUProject = Base.SelectProject(MountPath + @"SpellunkGameU\SpellunkGameU.csproj");
      var SpellunkGameUTarget = SpellunkSolution.SelectTarget("SpellunkGameU");
      var SpellunkGameUManifest = SpellunkGameUProject.SelectUwaManifest();

      var SpellunkVersionTimestamp = Base.SelectTimestamp(MountPath + @"SpellunkGame\Resources\Texts\Version.txt");
      var InvMacServer = Base.SelectMacServer("Calmac", "callanh", "");

      var WpfProcedure = Base.AddProcedure("Windows Desktop");
      WpfProcedure.AddTask("Build Program", T =>
      {
        var Configuration = Inv.BuildConfiguration.Release;
        var Platform = Inv.BuildPlatform.x86;

        T.CleanWpfTarget(SpellunkGameWTarget, Configuration, Platform);
        T.ApplyTimestamp('w', SpellunkVersionTimestamp);
        T.GenerateResources(SpellunkGameProject);
        T.BuildWpfTarget(SpellunkGameWTarget, Configuration, Platform);

        // TODO: zip and deploy or use innosetup?
      });

      var AndroidProcedure = Base.AddProcedure("Android");
      AndroidProcedure.AddTask("Build Program", T =>
      {
        var Configuration = Inv.BuildConfiguration.Release;
        var Platform = Inv.BuildPlatform.AnyCPU;

        T.CleanAndroidTarget(SpellunkGameATarget, Configuration, Platform);
        T.ApplyTimestamp('a', SpellunkVersionTimestamp);
        T.GenerateResources(SpellunkGameProject);
        T.IncrementAndroidVersion(SpellunkGameAManifest);
        T.BuildAndroidTarget(SpellunkGameATarget, Configuration, Platform);
        T.SignAndroidBundle(SpellunkGameATarget, Configuration, Platform);
        T.DeployAndroidBundle(SpellunkGameAProject, Configuration, Platform, DeployPath + "Spellunk", SpellunkGameAManifest);
      });

      var iOSProcedure = Base.AddProcedure("iOS");
      iOSProcedure.AddTask("Build Program", T =>
      {
        var Configuration = Inv.BuildConfiguration.Release;
        var Platform = Inv.BuildPlatform.iPhone;

        T.CleaniOSTarget(SpellunkGameITarget, Configuration, Platform, InvMacServer);
        T.ApplyTimestamp('i', SpellunkVersionTimestamp);
        T.GenerateResources(SpellunkGameProject);
        T.IncrementiOSVersion(SpellunkGameIManifest);
        T.BuildiOSTarget(SpellunkGameITarget, Configuration, Platform, InvMacServer);
        T.DeployiOSPackage(SpellunkGameIProject, Configuration, Platform, DeployPath + "Spellunk", SpellunkGameIManifest);
        //T.DeployiOSdSYM(SpellunkGameIProject, Configuration, Platform, DeployPath + "Spellunk", SpellunkGameIManifest);
      });

      var UwaProcedure = Base.AddProcedure("Universal Windows");
      UwaProcedure.AddTask("Build Program", T =>
      {
        var Configuration = Inv.BuildConfiguration.Release;
        var PlatformArray = new[] { Inv.BuildPlatform.x86, Inv.BuildPlatform.x64, Inv.BuildPlatform.ARM };

        T.CleanUwaTarget(SpellunkGameUTarget, Configuration, PlatformArray);
        T.ApplyTimestamp('u', SpellunkVersionTimestamp);
        T.GenerateResources(SpellunkGameProject);
        T.IncrementUwaVersion(SpellunkGameUManifest);
        T.BuildUwaTarget(SpellunkGameUTarget, Configuration, PlatformArray);
        T.DeployUwaPackage(SpellunkGameUProject, PlatformArray, DeployPath + "Spellunk", SpellunkGameUManifest);
      });

      var BlazorProcedure = Base.AddProcedure("Blazor WebAssembly");
      BlazorProcedure.AddTask("Build Program", T =>
      {
        T.ApplyTimestamp('b', SpellunkVersionTimestamp);
        T.GenerateResources(SpellunkGameProject);
        // TODO: Increment Blazor version?
      });
    }
  }
}
