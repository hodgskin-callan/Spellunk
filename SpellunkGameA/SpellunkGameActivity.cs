﻿namespace SpellunkGameA
{
  [Android.App.Activity(MainLauncher = true, NoHistory = true, Theme = "@android:style/Theme.NoTitleBar")]
  public sealed class SplashActivity : Inv.AndroidSplashActivity
  {
    protected override void Install()
    {
      this.ImageResourceId = SpellunkGameA.Resource.Drawable.Splash;
      this.AppCenterAppID = "4f45fbe6-d020-41e4-acf4-7172c8eb94d0";
      this.LaunchActivity = typeof(MainActivity);
    }
  }

  [Android.App.Activity(MainLauncher = false, ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    protected override void Install(Inv.Application Application)
    {
      Spellunk.Shell.Install(Application);
    }
  }
}