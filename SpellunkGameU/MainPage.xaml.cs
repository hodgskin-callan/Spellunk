﻿namespace SpellunkGameU
{
  public sealed partial class MainPage : Windows.UI.Xaml.Controls.Page
  {
    public MainPage()
    {
      this.InitializeComponent();

      Inv.UwaShell.AppCenterAppID = "7cc752f9-2994-4ff5-acba-9d8ab3d26a8a";
      Inv.UwaShell.Attach(this, Spellunk.Shell.Install);
    }
  }
}